#include "jptmonitoring.h"
#include <QCheckBox>
#include <QFormLayout>
#include <QQuickView>
#include <QUrl>

#define def_if_connected_to_cloud           if(gv_data_configu[0][def_pos_conf_db_status_cloud].toInt())
#define def_if_connected_to_modbus          if(gv_data_configu[0][def_pos_conf_db_status_modbus].toInt())
#define def_if_connected_to_pwm             if(gv_data_configu[0][def_pos_conf_db_status_pwm].toInt())
//#define def_if_connected_to_widget          if(gv_data_modules.length() > 0)
//#define def_if_connected_to_cloud_or_modbus if(gv_data_configu[0][def_pos_conf_db_status_modbus].toInt() || gv_data_configu[0][def_pos_conf_db_status_cloud].toInt())

#ifdef _COMPILE_GUI
QVector<QLCDNumber  *> gv_gui_raw_data;
QVector<QLCDNumber  *> gv_gui_ing_data;
QVector<QLineEdit   *> gv_equ_field;

QVector<QLabel  *> gv_gui_name_client;
QVector<QLabel  *> gv_gui_status_client;
QVector<QCheckBox *> gv_gui_send_client;
QVector<QLineEdit  *> gv_gui_ip_client;



// Mantener el control de despliegue de Widgets en el GridLayout.
int gv_count_rows(1);
int gv_count_cols(1);
int gv_count_node(0);

// Para realizar X columnas de Widgets.
int def_num_display_x_row_1(1);
int def_num_display_x_row_2(0);
#endif

// Contabilizar la cantidad de nodos que se destinaron.
int gv_num_nodes(0);

// Calcula los valores de ingenieria de los sensores, a partir del dato crudos.
QVector<jptabsvar  *>   gv_abs_sensor;

// Almacena la informacion proveniente de la base de datos.
QVector<QStringList >   gv_data_functio;
QVector<QStringList >   gv_data_configu; // [1] Como estos son de configuracion se puede reducir su tamano o volverlo una variable local y parametrizar el codigo con la informacion que este contiene.
QVector<QStringList >   gv_data_charact; //
QVector<QStringList >   gv_data_modules; // lo mismo que [1].
QVector<QStringList >   gv_data_pwms;    // lo mismo que [1].
QVector<QStringList >   gv_data_delayer; // lo mismo que [1].

QVector<QStringList  > gv_info_widgets;

QStringListModel* cbmodel;


//******************************************************************************************************************************
// Funcion principal                                                                                             [jptmonitoring]
//******************************************************************************************************************************
jptmonitoring::jptmonitoring(QWidget *parent) : QMainWindow(parent), ui(new Ui::jptmonitoring){
    ui->setupUi(this);



#ifdef _COMPILE_GUI


    //----------------------------------------------------------------------------------------------------------------
    // Conectar Botones de la interfaz
    //----------------------------------------------------------------------------------------------------------------
    // Actualizar las ecuaciones de calibracion en la base de datos, y en los sensores.
    //connect(ui->btn_update_equ, SIGNAL(clicked()), this, SLOT(calibrate_equation_btn()));
    // Guardar la base de datos en la memoria llamada JPT
    cbmodel=new QStringListModel();
    ui->comboBox->setModel(cbmodel);
    connect(ui->comboBox,SIGNAL(currentTextChanged(QString)),this,SLOT(update_devices_functions(QString)));
    connect(ui->btn_save_config,SIGNAL(clicked()),this,SLOT(save_txt_device()));




    connect(ui->btn_export_db_2,  SIGNAL(clicked()), this, SLOT(export_db_memory()));

    connect(ui->btn_shoot_siren,  SIGNAL(clicked()), this, SLOT(process()));

    //----------------------------------------------------------------------------------------------------------------
    // Configuracion [Adaptacion del tamano de la aplicacion al tamano del display utilizado]             [MainWindow]
    //----------------------------------------------------------------------------------------------------------------
    QScreen *screen   = QGuiApplication::primaryScreen();
    QRect  dim_screem = screen->geometry();
    this->setFixedSize(dim_screem.width()-80, dim_screem.height()-30);
    this->setWindowIcon(QIcon(":/init.png"));

    //----------------------------------------------------------------------------------------------------------------
    //Cargamos la lista de clientes permitidos en el sistema
    //----------------------------------------------------------------------------------------------------------------
    charge_clients();
    //----------------------------------------------------------------------------------------------------------------
    //cargamos la informacion de los clientes en la parte grafica de los settings.
    //----------------------------------------------------------------------------------------------------------------
    charge_graphics_clients_settings();

    //----------------------------------------------------------------------------------------------------------------
    //Cargamos la inforacion del area de sirenas.
    //----------------------------------------------------------------------------------------------------------------
    charge_graphics_clients_alarms();

    //----------------------------------------------------------------------------------------------------------------
    //connectamos el sistema se señales de tipo de sirena de envio
    //----------------------------------------------------------------------------------------------------------------
    connect_alarm_check();

    //----------------------------------------------------------------------------------------------------------------
    // Configuracion de la interfaz que permite seleccionar el puerto serie del xbee-datos          [jptserialdevices]
    //----------------------------------------------------------------------------------------------------------------
    gui_serial      = new jptserialdevices(this);

    connect(gui_serial, SIGNAL(was_opened(QStringList)), this, SLOT(serial_devices_selection(QStringList)));
    connect(ui->btn_gui_serial, SIGNAL(clicked()), this, SLOT(gui_open_serial()));
    //Recepcion de la MAC del Ftdi
    connect(gui_serial, SIGNAL(sendMacFtdi(QString)), this, SLOT(recepcionMacFtdi(QString)));
#endif

    //----------------------------------------------------------------------------------------------------------------
    // Configuracion de la base de datos y carga de valores                                              [jptdatabase]
    //----------------------------------------------------------------------------------------------------------------
    a_data_base = new jptdatabase(gv_name_data_base, gv_pass_data_base, gv_user_data_base, this);

#ifdef _COMPILE_MESSAGE
    connect(a_data_base, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status(QString)));
#endif
    connect(this, SIGNAL(send_message_to_database(QString,QString)), a_data_base, SLOT(insert_register(QString,QString)));

    a_data_base->init_database_connection();

    a_data_base->get_values(def_pos_table_conf, &gv_data_configu); // Informacion tabla Configurations
    //a_data_base->get_values(def_pos_table_char, &gv_data_charact); // Informacion tabla Characteristics
    a_data_base->get_values(def_pos_table_modu, &gv_data_modules); // Informacion tabla Modules [que reconozca que el modulo es el widget de corte .. en el widget en la trama de inicializacion agregar que se envie el nombre]
    //a_data_base->get_values(def_pos_table_pwms, &gv_data_pwms   ); // Informacion tabla Pwms
    a_data_base->get_values(def_pos_table_dela, &gv_data_delayer); // Informacion tabla Delayer

    set_chart_info();
    connect(ui->btn_update_to_charts,SIGNAL(clicked()),this,SLOT(update_chart_info()));

    //----------------------------------------------------------------------------------------------------------------
    // Vigila la conexion a la nube e internet                                                        [jptclientcloud]
    //----------------------------------------------------------------------------------------------------------------
    def_if_connected_to_cloud{
        configure_cloud();
    }
    charge_setings();

    //----------------------------------------------------------------------------------------------------------------
    // Nombre sobre la ventana principal                                                               [jptmonitoring]
    //----------------------------------------------------------------------------------------------------------------

    gv_num_nodes = gv_data_configu[0][def_pos_db_nume_node].toInt();
#ifdef _COMPILE_GUI
    setWindowTitle("JPT Monitoring V2.0: [Name Hub: " + gv_data_configu[0][def_pos_db_name_hub]+"]");
    def_num_display_x_row_1 = (int) gv_data_configu[0][def_pos_db_nume_node].toInt()/2;
    //////qDebug()<<def_num_display_x_row_1<<"mi cantidad de columnas es ";
    def_num_display_x_row_2 = def_num_display_x_row_1 - 1;
#endif

    //----------------------------------------------------------------------------------------------------------------
    // Crear la comunicacion con los widgets                                                           [jptconwidgets]
    //----------------------------------------------------------------------------------------------------------------
    //def_if_connected_to_widget{

    a_watch_status_widget = new jptconwidgets(this);
    //cargo los valoes de la lista de clientes y sus nombres
    a_watch_status_widget->set_ip_clients(ip_client_list);
    a_watch_status_widget->set_name_clients(name_client_list);
    a_watch_status_widget->set_status_client_list(status_client_list);
    a_watch_status_widget->set_send_client_list(send_client_list);
    //cargamos los valores de tiempo en la parte grafica
    charge_clients_times();


#ifdef _COMPILE_MESSAGE
    connect(a_watch_status_widget, SIGNAL(viewer_status(QString)), this, SLOT(viewer_siren(QString)));
#endif

    connect(a_watch_status_widget, SIGNAL(serial_data_new(QStringList)), this, SLOT(serial_data_new(QStringList)));

    connect(this, SIGNAL(send_message_to_widget(QStringList,QString,QString,QString)), a_watch_status_widget, SLOT(send_message_to_widget(QStringList,QString,QString,QString)));
    connect(this, SIGNAL(send_message_to_widget_raw(QString,QString,QString)), a_watch_status_widget, SLOT(send_message_to_widget_raw(QString,QString,QString)));

    connect(this, SIGNAL(send_message_to_widget_v22(QString)), a_watch_status_widget, SLOT(send_message_to_widget_v2(QString)));
    connect(this, SIGNAL(change_status_widget_send(QList<bool>)), a_watch_status_widget, SLOT(change_status_send(QList<bool>)));
    //funcion que termina todo el proceso de la sirena
    connect(this, SIGNAL(stop_all_sirens(QString)), a_watch_status_widget, SLOT(send_message_to_widget_v2(QString)));


    connect(a_watch_status_widget, SIGNAL(status_conection_widget(QList<bool>)), this, SLOT(change_conection_status_widget(QList<bool>)));
    connect(ui->btn_stop,SIGNAL(clicked()),this,SLOT(closed_sirens()));
    connect(ui->btn_start_broadcast,SIGNAL(clicked()),this,SLOT(start_broadcast()));
    connect(ui->btn_stop_broadcast,SIGNAL(clicked()),this,SLOT(stop_broadcast()));
    ui->btn_stop_broadcast->setEnabled(false);//el boton de cierre dle broadcast se inicia apagado
    connect(this,SIGNAL(start_broadcast_signal(QString)), a_watch_status_widget, SLOT(send_message_to_widget_v2(QString)));
    connect(this,SIGNAL(stop_broadcast_signal(QString)), a_watch_status_widget, SLOT(send_message_to_widget_v2(QString)));



    //----------------------------------------------------------------------------------------------------------------
    // Configuracion [a_serial_device: controla la comunicacion serial con xbee]                           [jptserial]
    //----------------------------------------------------------------------------------------------------------------
    a_serial_device = new jptserial(gv_data_configu[0][def_pos_conf_db_fact_radio],gv_data_configu[0][def_pos_conf_db_baud_rate].toInt(), this);

    consulta_equipos = new jptdevices();//inicio mi variable de consulta a la libreria.

    connect(a_serial_device, SIGNAL(send_trame_to_monitoring(int,int,int,QList<int>)), this, SLOT(serial_data_reciver(int,int,int,QList<int>)));




    connect(a_serial_device, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status(QString)));

    connect(a_serial_device, SIGNAL(serial_data(QStringList)), this, SLOT(serial_data_new(QStringList)));

    a_serial_device->init_serial_connection();


    //----------------------------------------------------------------------------------------------------------------
    // Configuracion [a_serial_device btn: controla la comunicacion serial con ftdi y el btoon de la base] [jptserial]
    //----------------------------------------------------------------------------------------------------------------
    validarTxtFTDI();//cargamos el valor inicial de la variable ftdibtn; si no existe la creamos el valor entonces seria de default
    //ahora iniciamos nuestra clase de jptserialbtn
    a_serial_device_btn= new jptserialbtn(getFtdiBtn(),gv_data_configu[0][def_pos_conf_db_baud_rate].toInt(), this);
    //ahora realizamos conexion para la visualizacion
    connect(a_serial_device_btn, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status(QString)));

    a_serial_device_btn->init_serial_connection();

    connect(a_serial_device_btn, SIGNAL(signalButttonFtdi()), this, SLOT(activeSirenFtdi()));
}
//******************************************************************************************************************************
// Funcion que envia los datos a las diferentes aplicaciones que requerieran de los datos                        [jptmonitoring]
//******************************************************************************************************************************
#define def_pos_date    0,10   // Posiciones que contienen la fecha
#define def_pos_time    11,-1  // Posiciones que contienen la hora
//******************************************************************************************************************************
void jptmonitoring::build_and_send_trame_to_connections(QString value_1, QString value_2, QString name_node){
    int lv_post_node = name_node.toInt()  - 1;

    QDateTime lv_now = QDateTime::currentDateTime();
    QString lv_time_send = lv_now.toString("yyyy/MM/dd hh:mm:ss");

    if(lv_post_node > -1){
        emit send_message_to_database("Node_" + name_node, "(raw_1,physic_1,raw_2,physic_2,date)VALUES('" + gv_abs_sensor[lv_post_node]->a_raw_sen_1 + "','" + value_1 + "','" + gv_abs_sensor[lv_post_node]->a_raw_sen_2 + "','" + value_2 + "','" + lv_time_send + "');");

        def_if_connected_to_pwm{
            if(gv_data_charact[lv_post_node][def_pos_char_db_pwm_var_1].toInt())
                emit send_message_to_pwm(value_1, gv_data_charact[lv_post_node][def_pos_char_db_pwm_pin_var_1]);
            if(gv_data_charact[lv_post_node][def_pos_char_db_pwm_var_2].toInt())
                emit send_message_to_pwm(value_2, gv_data_charact[lv_post_node][def_pos_char_db_pwm_pin_var_2]);
        }



        emit send_message_to_widget(gv_info_widgets[lv_post_node], name_node, value_1, value_2);


    }

}

void jptmonitoring::start_broadcast()
{
    //cuando inicia el start se debe mandar a cerrar primero el boton de start
    ui->btn_start_broadcast->setEnabled(false);
    //luego se debe cerrar la comunicacion con el sistema de alarma y todo lo que exista de el
    ui->groupBox_s_alarm->setEnabled(false);
    //luego mandaos una señal de cierra por si caso estaba sonado la alarma
    //closed_sirens();
    //ahora debo de emitir señal de inicio de broadcast
    start_broadcast_signal("Broadcast_Start");
    start_client_broadcast();
    //cuando la señal de broadcast se emitio, entonces abrimos el terminal java del broadcast
    ui->btn_stop_broadcast->setEnabled(true);

}

void jptmonitoring::stop_broadcast()
{
    //cuando cerramos la comunicacion broadcas, primero cerramos el servicio
    stop_broadcast_signal("Broadcast_Stop");
    //Ahora cerramos el terminal del cliente broadcast
    //luego prendemos el boton de start broadcas
    ui->btn_start_broadcast->setEnabled(true);
    //luego se prende el panel del audio
    ui->groupBox_s_alarm->setEnabled(true);
    //y listo el sistema de cambio de broadcast

}

void jptmonitoring::start_client_broadcast()
{
    ////qDebug()<<"init qprocess system";
    //lo primero que se hara es abrir un qprocess para que genere la instancia requerida
    QProcess *_Program = new QProcess();
    _Program->setProgram("python3");
    _Program->setArguments(QStringList() <<"/jpt/jptmonitoring/External/client_voice/dist/start_client.py");
    _Program->start();


}

void jptmonitoring::stop_client_broadcast()
{   //se mata el proceso de 3 maneras para evitar incomvenientes.
    system("sudo killall client_voice.jar");
    system("sudo killall java");
    system("sudo killall java -jar");

}


void jptmonitoring::set_chart_info()
{   //carga los valores de los datos.
    QStringList data_config=a_data_base->get_data_config();
    /*
    for(int i=0;i<data_config.size();i++)
    {
        ////qDebug()<<i<<"ste es mi dato"<<data_config[i];
    }*/
    ui->val_gt_1->setText(data_config[0]);
    ui->val_gt_2->setText(data_config[1]);
    ui->val_gt_3->setText(data_config[2]);
    ui->val_gt_4->setText(data_config[3]);

    ui->val_pluv_1->setText(data_config[4]);
    ui->val_pluv_2->setText(data_config[5]);
    ui->val_pluv_3->setText(data_config[6]);
    ui->val_pluv_4->setText(data_config[7]);
    ui->val_pluv_5->setText(data_config[8]);
    ui->val_pluv_6->setText(data_config[9]);

    ui->val_level_1->setText(data_config[10]);
    ui->val_level_2->setText(data_config[11]);
    ui->val_level_3->setText(data_config[12]);
    ui->val_level_4->setText(data_config[13]);
    ui->val_level_5->setText(data_config[14]);
    ui->val_level_6->setText(data_config[15]);


}

void jptmonitoring::update_chart_info()
{
    //ciclo que se realiza par actualizar el paquete de envio de datos.
    a_data_base->set_data_config(1,ui->val_gt_1->text());
    a_data_base->set_data_config(2,ui->val_gt_2->text());
    a_data_base->set_data_config(3,ui->val_gt_3->text());
    a_data_base->set_data_config(4,ui->val_gt_4->text());
    a_data_base->set_data_config(5,ui->val_pluv_1->text());
    a_data_base->set_data_config(6,ui->val_pluv_2->text());
    a_data_base->set_data_config(7,ui->val_pluv_3->text());
    a_data_base->set_data_config(8,ui->val_pluv_4->text());
    a_data_base->set_data_config(9,ui->val_pluv_5->text());
    a_data_base->set_data_config(10,ui->val_pluv_6->text());
    a_data_base->set_data_config(11,ui->val_level_1->text());
    a_data_base->set_data_config(12,ui->val_level_2->text());
    a_data_base->set_data_config(13,ui->val_level_3->text());
    a_data_base->set_data_config(14,ui->val_level_4->text());
    a_data_base->set_data_config(15,ui->val_level_5->text());
    a_data_base->set_data_config(16,ui->val_level_6->text());


}


void jptmonitoring::process()
{

    //////qDebug()<<"Entro al proceso ";
    msg_type=ui->comboBox_signal->currentIndex();
    QString signal_active;

    if(msg_type==0)
    {//Esta check la alarma 1.
        signal_active="alarma_num_1";
    }

    else if(msg_type==1)
    {//Esta check la alarma 2
        signal_active="alarma_num_2";
    }

    else if(msg_type==2)
    {//Esta check la alarma 3.
        signal_active="alarma_num_3";
    }
    else if(msg_type==3)
    {//Esta check la alarma 1.
        signal_active="alarma_num_4";
    }

    else if(msg_type==4)
    {//Esta check la alarma 1.
        signal_active="alarma_num_5";
    }

    else if(msg_type==5)
    {//Esta check la alarma 1.
        signal_active="alarma_record";
    }
    else
    {
        signal_active="null";
    }
    emit send_message_to_widget_v22(signal_active);


}

void jptmonitoring::change_conection_status_widget(QList<bool> status_list)
{
    //funcion que actualiza el vector de datos dentro del sistma
    status_client_list.clear();
    status_client_list=status_list;

    //ahora verifico el valor
    for (int i=0;i<status_client_list.size();i++) {
        if(status_client_list[i])
        {//si es verdadero seteo mi funcion en verdadero
            if(i==0)
            {
                ui->status_siren_1->setText("<font color='green'>Connected</font>");
                ui->alarm_1_state->setText("<font color='green'>Connected</font>");

            }
            if(i==1)
            {
                ui->status_siren_2->setText("<font color='green'>Connected</font>");
                ui->alarm_2_state->setText("<font color='green'>Connected</font>");

            }
            if(i==2)
            {
                ui->status_siren_3->setText("<font color='green'>Connected</font>");
                ui->alarm_3_state->setText("<font color='green'>Connected</font>");

            }
        }
        else
        {
            if(i==0)
            {   ui->status_siren_1->setText("<font color='red'>Disconnected</font>");
                ui->alarm_1_state->setText("<font color='red'>Disconnected</font>");


            }
            if(i==1)
            {   ui->status_siren_2->setText("<font color='red'>Disconnected</font>");
                ui->alarm_2_state->setText("<font color='red'>Disconnected</font>");
            }
            if(i==2)
            {   ui->status_siren_3->setText("<font color='red'>Disconnected</font>");
                ui->alarm_3_state->setText("<font color='red'>Disconnected</font>");


            }
        }
    }
}

void jptmonitoring::closed_sirens()
{//cerramos todas las sirenas
    emit stop_all_sirens("Stop");


}

void jptmonitoring::charge_setings()
{
    //cargamos los datos del sistema de envio
    ui->name_hub->setText(gv_data_configu[0][def_pos_db_name_hub]);
    ui->ip_cloud->setText(gv_data_configu[0][def_pos_conf_db_ip_cloud]);
    ui->port_cloud->setText(gv_data_configu[0][def_pos_conf_db_port_cloud]);

    //creo la conexion del sistema
    connect(ui->btn_update_settings,SIGNAL(clicked()),this,SLOT(update_settings()));

}

void jptmonitoring::update_settings()
{
    //sistema que actualiza los valores de envio a la nube
    //primero el sistema obtendra los valores actualies de la parte grarica.
    QString name_hub=ui->name_hub->text();
    QString ip_cloud=ui->ip_cloud->text();
    QString port_cloud=ui->port_cloud->text();



    a_data_base->update_ip_cloud(ip_cloud);
    a_data_base->update_hub_name(name_hub);
    a_data_base->update_port_cloud(port_cloud);


    //luego de este punto debemos de reiniciar los valores o sobre escribirlos
    gv_data_configu.clear();
    a_data_base->get_values(def_pos_table_conf, &gv_data_configu); // Se carga nuevamente Informacion tabla Configurations
    setWindowTitle("JPT Monitoring V2.0: [Name Hub: " + gv_data_configu[0][def_pos_db_name_hub]+"]");
    a_watch_status_cloud->set_ip_cloud(ip_cloud);
    int temp_port=port_cloud.toInt();
    a_watch_status_cloud->set_port_cloud(temp_port);
    a_watch_status_cloud->system_restart();

}

//******************************************************************************************************************************
// Funcion  encargada de cargar los datos de ip y nombre de los clientes en el sistema.
//******************************************************************************************************************************
void jptmonitoring::serial_data_reciver(int id_equipo,int id_target,int num_sensor,QList<int> sensores)
{

    ////qDebug()<<"recibio trama"<<id_equipo<<id_target<<num_sensor;

    //lo primero es verificar si el elemento de la lista de guardado es cero
    //si es cero debo registrar mi funcion
    if(lista_equipos.isEmpty())
    {
        //si esta vacia registro la primera trama en el sistema
        lista_equipos.append(new jptdevices());
        //lista_equipos[0]->init_device(3,id_target,num_sensor,sensores);

        connect(lista_equipos[0],SIGNAL(combo_box_element(QString)),this,SLOT(charge_devices_functions(QString)));
        lista_equipos[0]->init_device(id_equipo,id_target,num_sensor,sensores);

        //////qDebug()<<"Mi lista ahora tiene tamaño"<<lista_equipos.size();
        //para prueba consulto la lista de variables del sistema

        connect(lista_equipos[0],SIGNAL(signal_to_viewer_reciver(QString,QString,QString,QString,QString,QString,QString,QString,int,QList<double>)),this,SLOT(data_from_devices(QString,QString,QString,QString,QString,QString,QString,QString,int,QList<double>)));

        serial_data_reciver(id_equipo,id_target,num_sensor,sensores);



    }
    else
    {//si mi puntero no esta vacio el ciclo se debe de encargar de verificar si ya existe
        //otro vector o arreglo con el mismo nombre
        bool existe_dentro=false;
        int numero_dentro_vector;
        for(int i=0;i<lista_equipos.size();i++)//recorre toda la lista
        {
            if(lista_equipos[i]->get_id_targeta()==id_target)
            {
                //si pasa esto el equipo ya esta registrado en el sistema
                numero_dentro_vector=i;
                existe_dentro=true;

                break;
            }

        }
        //Nota: SOlo se hara recursividad cuando se hace registro de datos en caso contrario se carga la informaicon al sistema

        if(existe_dentro==true)
        {
            //////qDebug()<<"Equipo ya registrado en sistema";
            //como el equipo ya esta registrado solo actualizo el valor de los datos dentro del sistema
            lista_equipos[numero_dentro_vector]->set_sensor_raw(sensores);

            lista_equipos[numero_dentro_vector]->start();


        }
        else
        {
            //Proceso de nuevo registro del sistema
            int temp_tam=lista_equipos.size();
            lista_equipos.append(new jptdevices());
            connect(lista_equipos[temp_tam],SIGNAL(combo_box_element(QString)),this,SLOT(charge_devices_functions(QString)));

            lista_equipos[temp_tam]->init_device(id_equipo,id_target,num_sensor,sensores);
            // ////qDebug()<<"Mi lista ahora tiene tamaño"<<lista_equipos.size();
            connect(lista_equipos[temp_tam],SIGNAL(signal_to_viewer_reciver(QString,QString,QString,QString,QString,QString,QString,QString,int,QList<double>)),this,SLOT(data_from_devices(QString,QString,QString,QString,QString,QString,QString,QString,int,QList<double>)));
            serial_data_reciver(id_equipo,id_target,num_sensor,sensores);

        }
    }


}



void jptmonitoring::charge_devices_functions(QString data_txt)
{
    //cargamos los dattos iniciales del sistema
    //primero agrego el elemento a la combobox
    //antes de crear el elemento verifico que el sistema no contenga uno de igual nombre
    QStringList item_list=cbmodel->stringList();
    bool exist_item=false;
    if(item_list.size()>0 )
    {

        for(int i=0;i<item_list.size();i++)
        {
            //recorro mi lista de combo box
            if(item_list[i]==data_txt)
            {
                //si esto sucede entonces el elemento ya esta en la lista
                exist_item=true;
            }
        }
    }
    if(!exist_item)
    {//se adiciona el elemento a ala lista
        item_list.append(data_txt);
        cbmodel->setStringList(item_list);
        ui->comboBox->setModel(cbmodel);



    }


}

void jptmonitoring::update_devices_functions(QString _data)
{


    //ahora parceo el valor de la data obtenida
    QStringList _list=_data.split(">>");
    //El primer elemento es el id y el segundo es el nombre
    ui->label_select_device->setText(_list[0]);
    ui->text_device->setPlainText("");
    charge_functions_modify(_list[0]);

}

void jptmonitoring::charge_functions_modify(QString my_int)
{

    //Se carga el path de conexion o almacenmiento del archivo .txt
    QString path_source ="/jpt/jptmonitoring/External/Devices/ID-"+my_int+".txt";

    QFile file(path_source);//leo el archivo si existe
    if(!file.exists())
    {
        ////qDebug() << "NO existe el archivo de funciones de transferencia de equipos: ID-"<<my_int;

    }
    else
    {//leo mi archivo
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&file);

            QString data=stream.readAll();

            ui->text_device->setPlainText(data);

        }
        file.close();
    }// fin del ciclo else

}

void jptmonitoring::save_txt_device()
{
    //obtengo el nombre del device
    QString name_device=ui->label_select_device->text();


    //Se carga el path de conexion o almacenmiento del archivo .txt
    QString path_source ="/jpt/jptmonitoring/External/Devices/ID-"+name_device+".txt";

    QFile file(path_source);//leo el archivo si existe
    if(!file.exists())
    {
        ////qDebug() << "NO existe el archivo de funciones de transferencia de equipos: ID-"<<name_device;

    }
    else
    {//leo mi archivo
        if(file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QTextStream datosArchivo(&file);
            datosArchivo <<ui->text_device->toPlainText()<<endl;


        }
        file.close();
    }// fin del ciclo else


    //ahora emito señal de actualizacion de funciones
    for(int i=0;i<lista_equipos.size();i++){
        QString valor_target=QString::number(lista_equipos[i]->get_id_targeta());//obtengo el valor del id de targeta
        //se compara el valor con el id de targeta que se tiene
        if(name_device==valor_target)
        {
            //si son iguales atualizo la lista de variables o funciones de correccion.
            lista_equipos[i]->update_correct_f();

        }


    }
}

void jptmonitoring::data_from_devices(QString trame_time,QString my_device,QString msg_cabecera,QString msg_labels,QString msg_raw_data,QString msg_ing_data,QString data_cloud_database,QString string_id_target,int get_num_sensor,QList<double> list_ing_data)
{
    viewer_data(msg_cabecera);
    viewer_data(msg_labels);
    viewer_data("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

    QString m_device=my_device;


    //insertamos la data en la base de datos
    //organizo la query
    QString sqlite_query="";
    sqlite_query="(m_datetime,m_device,m_data,verify_send) VALUES('"+msg_ing_data+"',";
    sqlite_query=sqlite_query+"'"+m_device+"',";
    sqlite_query=sqlite_query+"'"+data_cloud_database+"','f')";
    a_data_base->insert_register("Nodex",sqlite_query);

    //Enviamos la data a la nube

    QString data_to_cloud="";

    //////qDebug()<<trame_time<<"Data a modificar";

    //:{"device_id"
    int id_hub=50000;
    //QString temp_data=":{\"hub_id\":\""+QString::number(id_hub)+"\",";
    QString temp_data=":{\"hub_id\":\""+gv_data_configu[0][def_pos_db_name_hub]+ "\",";
    temp_data=temp_data+"\"timestamp\":\""+trame_time+"\",";
    temp_data=temp_data+"\"device_id\"";
    data_to_cloud=data_cloud_database.replace(":{\"device_id\"",temp_data);
    //////qDebug()<<data_to_cloud;

    emit  send_message_to_cloud(data_to_cloud);
    data_relational_database(string_id_target,get_num_sensor,list_ing_data);
    viewer_data(data_to_cloud);
    viewer_data("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
    //luego de emitir la data a la nube la debo pasar a un sistema en el cual se guarde en la base de datos.



}

void jptmonitoring::data_relational_database(QString string_id_target,int get_num_sensor,QList<double> list_ing_data)
{
    //funcion encargada de enviar os datos a la base de datos relacional
    //Lo primero es verificar si existe el identificador dentro de la base de configuracion
    ////qDebug()<<"DATA RELATIONAL DATABASE";
    QString table_name=a_data_base->table_from_device(string_id_target,get_num_sensor,list_ing_data);

}

int contador_filas=0;
void jptmonitoring::viewer_data(QString data)
{
    ui->my_viewer_status->setTextColor(QColor(255,255,255));
    ui->my_viewer_status->append(data);

    ++contador_filas;
    if(contador_filas> 1000){
        ui->my_viewer_status->clear();
        contador_filas= 0;
    }


    //////qDebug() << data << "\n";

}

int contador_filas_siren=0;
void jptmonitoring::viewer_siren(QString data)
{


    ui->siren_viewer->setTextColor(QColor(255,255,255));
    ui->siren_viewer->append(data);

    ++contador_filas_siren;
    if(contador_filas_siren> 100){
        ui->my_viewer_status->clear();
        contador_filas_siren= 0;
    }


    //////qDebug() << data << "\n";

}

//******************************************************************************************************************************
// Funcion  encargada de cargar los datos de ip y nombre de los clientes en el sistema.
//******************************************************************************************************************************
void jptmonitoring::charge_clients()
{

    //Se carga el path de conexion o almacenmiento del archivo .txt con la ip.
    QString path_source = "/jpt/jptmonitoring/External/clients_data.txt";

    QFile file(path_source);
    if(!file.exists()){
        ////qDebug() << "NO existe el archivo de almacenamiento de clientes. "<<path_source;
        ////qDebug()<<"Se procede a crear el archivo";
        //como no existe el archivo procedemos a generar la cadena por primera vez.
        QString json_clients="";
        json_clients="{\"client1\": {\"ip\": \"192.168.1.2\",\"path\": \"siren_1\"},";
        json_clients=json_clients+"\"client2\": {  \"ip\": \"192.168.1.3\",\"path\": \"Siren_2\"},";
        json_clients=json_clients+"\"client3\": {\"ip\": \"192.168.1.4\",\"path\": \"Siren_3\"} }";

        if(!QDir("External").exists() )
        {
            //si no existe el directorio se crea.
            ////qDebug()<<"No existe el directorio de almacenamiento External, se procede a crearse";
            QDir().mkdir("External");
            ////qDebug()<<"Directorio External Creado";
            QFile archivo("External/clients_data.txt");
            ////qDebug()<<"Archivo de clientes Creado";
            if(archivo.open(QIODevice::WriteOnly | QIODevice::Text)){
                QTextStream datosArchivo(&archivo);
                datosArchivo << json_clients<<endl;
                //////qDebug()<<json_clients;

            }
            archivo.close();
            charge_clients();

        }
        else {
            QFile archivo("External/clients_data.txt");
            ////qDebug()<<"Archivo clientes Creado";
            if(archivo.open(QIODevice::WriteOnly | QIODevice::Text)){
                QTextStream datosArchivo(&archivo);
                datosArchivo << json_clients<<endl;
                ////qDebug()<<json_clients;

            }
            archivo.close();
            charge_clients();

        }

    }
    else{
        ////qDebug() << path_source <<" encontrado...";
        //realiza esta funcion cuando ha encontrado el archivo de informacion de los clientes
        QString line;//line from file to save the data.
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&file);

            QString data=stream.readAll();

            //ya se obtuvo la informacion del Json es tiempo de parsear la misma.
            data=data.replace("\n","");
            //////qDebug()<<data;
            //antes de iniciar el parseo limpio mis variables de sistema

            QJsonDocument lv_message_json = QJsonDocument::fromJson(data.toUtf8());
            QJsonObject   lv_trama_json   = lv_message_json.object();
            //////qDebug()<<lv_trama_json.length();
            QStringList parce_list_label=lv_trama_json.keys();

            if(!ip_client_list.isEmpty()){
                ip_client_list.clear();
            }
            if(!name_client_list.isEmpty()){
                name_client_list.clear();
            }

            for(int i=0;i<lv_trama_json.length();i++)
            {

                //////qDebug()<<"objeto :"<<parce_list_label[i].toUtf8();
                QString name_parce=parce_list_label[i].toUtf8();
                QJsonObject data_client=lv_trama_json.value(name_parce).toObject();
                ////qDebug()<<data_client.value("ip").toString();
                //se carga la ip
                ip_client_list.append(data_client.value("ip").toString());
                name_client_list.append(data_client.value("path").toString());

            }

        }
        file.close();

        //



    }


}
//******************************************************************************************************************************
// Funcion  encargada de cargar los datos de las siernas en la parte grafica de los settings
//******************************************************************************************************************************
void jptmonitoring::charge_graphics_clients_settings()
{
    //lo primero para carga datos de la parte grafica es obtener la cantidad de elementos del sistema
    int _clients=ip_client_list.length();
    //se setea el ip de cada sirena en la parte grafica
    ui->ip_siren_1->setText(ip_client_list[0]);
    ui->ip_siren_2->setText(ip_client_list[1]);
    ui->ip_siren_3->setText(ip_client_list[2]);


    //ahora se procede a generar las variables de status para cada sirena
    //estas variables se inician en false por defecto

    for(int i=0;i<_clients;i++)
    {
        status_client_list.append(false);

    }

    //luego de cargado el status de cada sirena, se procede a setear la nformacion en la parte grafica

    if(status_client_list[0])
    {

        ui->status_siren_1->setText("<font color='green'>Connected</font>");
        ui->alarm_1_state->setText("<font color='green'>Connected</font>");
    }
    else
    {
        ui->status_siren_1->setText("<font color='red'>Disconnected</font>");
        ui->alarm_1_state->setText("<font color='red'>Disconnected</font>");
    }
    if(status_client_list[1])
    {

        ui->status_siren_2->setText("<font color='green'>Connected</font>");
        ui->alarm_2_state->setText("<font color='green'>Connected</font>");
    }
    else
    {
        ui->status_siren_2->setText("<font color='red'>Disconnected</font>");
        ui->alarm_2_state->setText("<font color='red'>Disconnected</font>");
    }
    if(status_client_list[2])
    {

        ui->status_siren_3->setText("<font color='green'>Connected</font>");
        ui->alarm_3_state->setText("<font color='green'>Connected</font>");
    }
    else
    {
        ui->status_siren_3->setText("<font color='red'>Disconnected</font>");
        ui->alarm_3_state->setText("<font color='red'>Disconnected</font>");
    }


}
//******************************************************************************************************************************
// Funcion encargada de cargar los datos de la sirens en la parte de monitoreo de alarmas.
//******************************************************************************************************************************

void jptmonitoring::charge_graphics_clients_alarms()
{
    //lo primero para carga datos de la parte grafica es obtener la cantidad de elementos del sistema
    int _clients=ip_client_list.length();

    //ahora se procede a generar las variables de estado de envio de cada sirena.
    //estas variables se inician en verdadero por defecto

    for(int i=0;i<_clients;i++)
    {
        send_client_list.append(true);

    }


    //cargadas las funciones se procede a cargar los estados graficos.

    if(send_client_list[0])
    {

        ui->check_siren_1->setChecked(true);
        //seteamos la imagen del sistema
        ui->label_siren_1_sound->setPixmap(QPixmap(":/sound_on.png") );

    }
    else
    {
        ui->check_siren_1->setChecked(false);
        ui->label_siren_1_sound->setPixmap(QPixmap(":/sound_off.png") );
    }
    if(send_client_list[1])
    {
        ui->check_siren_2->setChecked(true);
        ui->label_siren_2_sound->setPixmap(QPixmap(":/sound_on.png") );
    }
    else
    {
        ui->check_siren_2->setChecked(false);
        ui->label_siren_2_sound->setPixmap(QPixmap(":/sound_off.png") );

    }
    if(send_client_list[2])
    {

        ui->check_siren_3->setChecked(true);
        ui->label_siren_3_sound->setPixmap(QPixmap(":/sound_on.png") );
    }
    else
    {
        ui->check_siren_3->setChecked(false);
        ui->label_siren_3_sound->setPixmap(QPixmap(":/sound_off.png") );
    }
    //en este punto genero las conecciones al sistema de eventos de checkeo
    connect(ui->check_siren_1,SIGNAL(stateChanged(int)),this,SLOT(check_siren_1(int)));
    connect(ui->check_siren_2,SIGNAL(stateChanged(int)),this,SLOT(check_siren_2(int)));
    connect(ui->check_siren_3,SIGNAL(stateChanged(int)),this,SLOT(check_siren_3(int)));


}

//******************************************************************************************************************************
//Funciones encargada del cambio de la interfax grafica para los estados de las sirenas
//******************************************************************************************************************************
void jptmonitoring::check_siren_1(int entrada)
{
    //////qDebug()<<entrada;
    //sistema que cambia entre parametros
    if(entrada==2)
    {

        ui->label_siren_1_sound->setPixmap(QPixmap(":/sound_on.png") );
        //cambio la parte grafica y el vector de datos
        send_client_list[0]=true;
    }
    else
    {
        ui->label_siren_1_sound->setPixmap(QPixmap(":/sound_off.png") );
        send_client_list[0]=false;
    }
    emit change_status_widget_send(send_client_list);


}
void jptmonitoring::check_siren_2(int entrada)
{
    //////qDebug()<<entrada;
    //sistema que cambia entre parametros
    if(entrada==2)
    {

        ui->label_siren_2_sound->setPixmap(QPixmap(":/sound_on.png") );
        send_client_list[1]=true;
    }
    else
    {
        ui->label_siren_2_sound->setPixmap(QPixmap(":/sound_off.png") );
        send_client_list[1]=false;
    }
    emit change_status_widget_send(send_client_list);

}
void jptmonitoring::check_siren_3(int entrada)
{
    //////qDebug()<<entrada;
    //sistema que cambia entre parametros
    if(entrada==2)
    {

        ui->label_siren_3_sound->setPixmap(QPixmap(":/sound_on.png") );
        send_client_list[2]=true;
    }
    else
    {
        ui->label_siren_3_sound->setPixmap(QPixmap(":/sound_off.png") );
        send_client_list[2]=false;
    }
    emit change_status_widget_send(send_client_list);

}

//cuando e checkea una señal se debe de emitir una señal con la informacion hacia loswidgt de actualizacion de data


//******************************************************************************************************************************
//Funciones encargadas del cambio de la interfaz grafica para los estados del envio de alarmas
//******************************************************************************************************************************

void jptmonitoring::connect_alarm_check()
{
    //ui->alarma_check_1->setChecked(true);
    msg_type=1;
    //connect(ui->alarma_check_1,SIGNAL(stateChanged(int)),this,SLOT(alarma_1_check(int)));
    //connect(ui->alarma_check_2,SIGNAL(stateChanged(int)),this,SLOT(alarma_2_check(int)));
    //connect(ui->alarma_check_3,SIGNAL(stateChanged(int)),this,SLOT(alarma_3_check(int)));
    //connect(ui->alarma_check_4,SIGNAL(stateChanged(int)),this,SLOT(alarma_4_check(int)));
    //connect(ui->alarma_check_5,SIGNAL(stateChanged(int)),this,SLOT(alarma_5_check(int)));
    //connect(ui->record_sound_check,SIGNAL(stateChanged(int)),this,SLOT(record_sound_check(int)));



}


void jptmonitoring::charge_clients_times()
{
    sound_time=30;
    repeat_times=1;
    ui->sound_time->setValue(sound_time);
    ui->set_repeat->setValue(repeat_times);

    //se carga el valor en el sistema de sirenas en los widgets.
    a_watch_status_widget->set_sound_time(sound_time);
    a_watch_status_widget->set_repeat_times(repeat_times);
    //generamos las conexiones del sistema de tiempos
    connect(ui->set_repeat,SIGNAL(valueChanged(int)),this,SLOT(update_repeat_time(int)));
    connect(ui->sound_time,SIGNAL(valueChanged(int)),this,SLOT(update_sound_time(int)));

}

//funcion que actualiza los tiempos de sonido  y repeticion del sistema de alertas
void jptmonitoring::update_repeat_time(int times)
{
    repeat_times=times;
    a_watch_status_widget->set_repeat_times(repeat_times);
}
void jptmonitoring::update_sound_time(int time)
{
    sound_time=time;
    a_watch_status_widget->set_sound_time(sound_time);
}

void jptmonitoring::generate_grafic_clients_settings()
{
    //lo primero es obtener la cantidad de elementos que se poseen
    int clients_list=ip_client_list.length();

    for(int i=0;i<clients_list;i++)
    {
        gv_gui_name_client << new QLabel(name_client_list[i]);
        gv_gui_ip_client << new QLineEdit(ip_client_list[i]);

        status_client_list.append(false);
        send_client_list.append(true);//inicio todas las funciones de envio de datos en positivo
    }
    //ahora se procede a generer los elementos graficos del sistema
    //creamos los titulos del sistema
    ui->ip_siren_1->setText(ip_client_list[0]);
    ui->ip_siren_2->setText(ip_client_list[1]);
    ui->ip_siren_3->setText(ip_client_list[2]);

    if(status_client_list[0])
    {

        ui->status_siren_1->setText("<font color='green'>Connected</font>");
    }
    else
    {
        ui->status_siren_1->setText("<font color='red'>Disconnected</font>");
    }
    if(status_client_list[1])
    {

        ui->status_siren_2->setText("<font color='green'>Connected</font>");
    }
    else
    {
        ui->status_siren_2->setText("<font color='red'>Disconnected</font>");
    }
    if(status_client_list[2])
    {

        ui->status_siren_3->setText("<font color='green'>Connected</font>");
    }
    else
    {
        ui->status_siren_3->setText("<font color='red'>Disconnected</font>");
    }

}

//******************************************************************************************************************************
// Funcion que construye la trama que se enviara al servidor nube                                                [jptmonitoring]
//******************************************************************************************************************************
QString jptmonitoring::build_trame_cloud(QString value_1, QString value_2, QString name_node, QString date){

    int lv_post_node = name_node.toInt()  - 1;


    // Se puede modificar para que en la base de datos se seleccione como se enviara la trama a la nube. Json o Normal.

    QString lv_trame_server("&&\n0101");

    if(lv_post_node > -1){
        //if(gv_data_charact[lv_post_node][def_pos_char_db_name_var_1] == "null")
        //if(gv_data_charact[lv_post_node][def_pos_char_db_name_var_2] == "null")

        lv_trame_server += gv_data_configu[0][def_pos_db_name_hub]                  + "\n0105";
        lv_trame_server += date.mid(def_pos_date).replace("/","")                   + "CST\n0106";
        lv_trame_server += date.mid(def_pos_time)                                   + "\n0110Node";
        lv_trame_server += name_node                                                + "\n0111";
        lv_trame_server += gv_data_charact[lv_post_node][def_pos_char_db_name_var_1]+ "\n0112";
        lv_trame_server += gv_data_charact[lv_post_node][def_pos_char_db_unit_var_1]+ "\n0113";
        lv_trame_server += value_1                                                  + "\n0114Node";
        lv_trame_server += name_node                                                + "\n0115";
        lv_trame_server += gv_data_charact[lv_post_node][def_pos_char_db_name_var_2]+ "\n0116";
        lv_trame_server += gv_data_charact[lv_post_node][def_pos_char_db_unit_var_2]+ "\n0117";
        lv_trame_server += value_2                                                  + "\n!!";
    }else{
        lv_trame_server += gv_data_configu[0][def_pos_db_name_hub]                  + "\n0105";
        lv_trame_server += date.mid(def_pos_date).replace("/","")                   + "CST\n0106";
        lv_trame_server += date.mid(def_pos_time)                                   + "\n0110Node";
        lv_trame_server += name_node                                                + "\n0111";
        lv_trame_server += "Voltage\n0112";
        lv_trame_server += "Volt\n0113";
        lv_trame_server +=  value_1 + "\n0114Node";
        lv_trame_server += "0000\n0115";
        lv_trame_server += "Current\n0116";
        lv_trame_server += "Amp\n0117";
        lv_trame_server +=  value_2 + "\n!!";
    }

    return lv_trame_server;
}

//******************************************************************************************************************************
// Funciones que inicializa las variables necesarias para el programa                                            [jptmonitoring]
//******************************************************************************************************************************
//******************************************************************************************************************************
// Exportar la base de datos                SIGNAL[btn_export_db(GUI), pul_export_db(Fisico){Thread} - SLOT] -> Ejecuta QThread.
//******************************************************************************************************************************
void jptmonitoring::export_db_memory(){
    jptexportdb *lv_export_db = new jptexportdb(gv_data_configu[0][def_pos_db_name_hub]);
#ifdef _COMPILE_MESSAGE
    connect(lv_export_db, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status(QString)));
#endif
    lv_export_db->start();
}
#ifdef _COMPILE_GUI
//******************************************************************************************************************************
// Cuando se cambia las ecuaciones de calibracion                                                  SIGNAL[gv_gui_btn_equ - SLOT]
//******************************************************************************************************************************
void jptmonitoring::calibrate_equation_btn(){
    for(int f_num_node = 0; f_num_node < gv_num_nodes; ++f_num_node){
        int lv_s1 = (f_num_node * 2);
        int lv_s2 = lv_s1 + 1;

        QString lv_s1_equ = gv_equ_field[lv_s1]->text();
        QString lv_s2_equ = gv_equ_field[lv_s2]->text();

        if(lv_s1_equ != gv_data_functio[f_num_node][def_pos_db_function_S1_C]){
            if(lv_s1_equ.contains("w")){
#ifdef _PROVE_EQUATION
                if(prove_equation(lv_s1_equ)){
#endif
                    gv_data_functio[f_num_node][def_pos_db_function_S1_C] = lv_s1_equ;
                    gv_abs_sensor[f_num_node]->set_correc_calib(gv_data_functio[f_num_node][def_pos_db_function_S1_C], def_sensor_1_thr);
#ifdef _PROVE_EQUATION
                    a_data_base->update_register("Functions", QString::number(f_num_node + 1), "transfer_c1='" + lv_s1_equ + "'");
                    gv_equ_field[lv_s1]->setStyleSheet("background-color: rgb(50,200,50,100)");
                }else
                    gv_equ_field[lv_s1]->setStyleSheet("background-color: rgb(200,50,50,100)");
#endif
            }else
                gv_equ_field[lv_s1]->setStyleSheet("background-color: rgb(200,50,50,100)");
        }
#ifdef _PROVE_EQUATION
        else
            gv_equ_field[lv_s1]->setStyleSheet("background-color: white");
#endif

        if(lv_s2_equ != gv_data_functio[f_num_node][def_pos_db_function_S2_C]){
            if(lv_s2_equ.contains("w")){
#ifdef _PROVE_EQUATION
                if(prove_equation(lv_s2_equ)){
#endif
                    gv_data_functio[f_num_node][def_pos_db_function_S2_C] = lv_s2_equ;
                    gv_abs_sensor[f_num_node]->set_correc_calib(gv_data_functio[f_num_node][def_pos_db_function_S2_C], def_sensor_2_thr);
#ifdef _PROVE_EQUATION
                    a_data_base->update_register("Functions", QString::number(f_num_node + 1), "transfer_c2='" + lv_s2_equ + "'");
                    gv_equ_field[lv_s2]->setStyleSheet("background-color: rgb(50,200,50,100)");
                }else
                    gv_equ_field[lv_s2]->setStyleSheet("background-color: rgb(200,50,50,100)");
#endif
            }else
                gv_equ_field[lv_s2]->setStyleSheet("background-color: rgb(200,50,50,100)");
        }
#ifdef _PROVE_EQUATION
        else
            gv_equ_field[lv_s2]->setStyleSheet("background-color: white");
#endif
    }
}
#ifdef _PROVE_EQUATION
//******************************************************************************************************************************
// Permite probar la ecuaciones que esten bien escritas                                                          [jptmoitoring]
//******************************************************************************************************************************
bool jptmonitoring::prove_equation(QString equation){
    bool validate_equ = true;
    try{
        equation.replace("w", "1");
        ValueList       lv_vlist;
        FunctionList    lv_flist;
        lv_vlist.AddDefaultValues();
        lv_flist.AddDefaultFunctions();

        Expression lv_e;
        lv_e.SetValueList(&lv_vlist);
        lv_e.SetFunctionList(&lv_flist);
        lv_e.Parse(equation.toStdString());
    }catch(...){
        validate_equ = false;
    }
    return validate_equ;
}
#endif
//******************************************************************************************************************************
// Abrir la configuracion del puerto serial                                                             [GUI - jptserialdevices]
//******************************************************************************************************************************
void jptmonitoring::gui_open_serial(){
    gui_serial->setModal(true);
    gui_serial->show();
    gui_serial->raise();
    gui_serial->activateWindow();
}
//******************************************************************************************************************************
// Cuando se selecciona un nuevo puerto serial, se cargan los parametros                  SIGNAL-SLOT   [GUI - jptserialdevices]
//******************************************************************************************************************************
void jptmonitoring::serial_devices_selection(QStringList serial_device){
    gv_data_configu[0][def_pos_conf_db_fact_modem    ] = serial_device[0];
    gv_data_configu[0][def_pos_conf_db_fact_radio    ] = serial_device[1];
    gv_data_configu[0][def_pos_conf_db_fact_modbus   ] = serial_device[2];
    gv_data_configu[0][def_pos_conf_db_baud_rate     ] = serial_device[3];
    a_serial_device->set_radio_factory_and_baud(gv_data_configu[0][def_pos_conf_db_fact_radio],gv_data_configu[0][def_pos_conf_db_baud_rate].toInt());
    a_data_base->update_register("Configurations", "mdb_baud='"+ serial_device[3] +"',fac_modem='"+ serial_device[0]+"',fac_radio='" + serial_device[1]+ "',fac_mdb='"+ serial_device[2] + "'");
}
#endif
//******************************************************************************************************************************
// Datos que se reciben del puerto serial                                                     SIGNAL-SLOT    [Radio - jptserial]
//******************************************************************************************************************************
void jptmonitoring::serial_data_new(QStringList data_serial){
    int lv_index(0);
    bool ok_v = true;
    lv_index    = (data_serial[def_pos_rd_name_node].toInt(&ok_v, 16) - 1);

#ifdef _FRAG_BITS_NAME_NODE
    int lv_name_hub(0);
    lv_name_hub = (data_serial[def_pos_rd_name_hubs].toInt());
#endif

    if(lv_index == 1)
#ifdef _COMPILE_MESSAGE
        viewer_status("[<] Serial " + data_serial[def_pos_rd_name_node]);
#endif

    if(lv_index < gv_num_nodes){
#ifdef _COMPILE_GUI
        int lv_index_par    = lv_index * 2;
        int lv_index_impar  = lv_index_par + 1;
#endif
        //cargo mis datos raw para el sensor, por ejemplo el nodo lv_index en mi hilo de datos.
        gv_abs_sensor[lv_index]->a_raw_sen_1 = data_serial[def_pos_rd_value_1];

        gv_abs_sensor[lv_index]->a_raw_sen_2 = data_serial[def_pos_rd_value_2];

#ifdef _COMPILE_GUI
        gv_gui_raw_data[lv_index_par  ]->display(data_serial[def_pos_rd_value_1]);
        gv_gui_raw_data[lv_index_impar]->display(data_serial[def_pos_rd_value_2]);
#endif
        emit send_message_to_widget_raw(QString::number(lv_index + 1), data_serial[def_pos_rd_value_1], data_serial[def_pos_rd_value_2]);
        gv_abs_sensor[lv_index]->start();

    }
}


#ifdef _COMPILE_MESSAGE
//******************************************************************************************************************************
// Visualizar el estado del programa                                                                             [jptmonitoring]
//******************************************************************************************************************************
int count_val = 0;

void jptmonitoring::viewer_status(QString status){
#ifdef _COMPILE_GUI

    ui->tx_gui_status->setTextColor(QColor(255,255,255));
    ui->tx_gui_status->append(status);


    ++count_val;
    if(count_val > 1000){
        ui->tx_gui_status->clear();
        count_val = 0;
    }

#else
    //////qDebug() << status << "\n";
#endif
}
#endif
//******************************************************************************************************************************
// Destructor                                                                                                    [jptmonitoring]
//******************************************************************************************************************************
jptmonitoring::~jptmonitoring(){
#ifdef _COMPILE_GUI
    delete ui;
#endif
}

//******************************************************************************************************************************
// Funcion que configura los parametros de la comunicacion con la nube                                           [jptmonitoring]
//******************************************************************************************************************************
void jptmonitoring::configure_cloud(){
    a_watch_status_cloud = new jptclientcloud(gv_data_configu[0][def_pos_conf_db_ip_cloud], gv_data_configu[0][def_pos_conf_db_port_cloud].toInt(), this);
#ifdef _COMPILE_MESSAGE
    connect(a_watch_status_cloud, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status(QString)));
#endif
    connect(this, SIGNAL(send_message_to_cloud(QString)), a_watch_status_cloud, SLOT(stack_data(QString)));

    connect(a_watch_status_cloud, SIGNAL(value_volts_and_current_signal(QString,QString,QString)), this, SLOT(build_and_send_trame_to_connections(QString,QString,QString)));

    a_watch_dog = new jptwdog();
    connect(a_watch_status_cloud, SIGNAL(restart_beagle_signal_ethernet()), a_watch_dog, SLOT(reset_network()));
    a_watch_dog->start();
}

void jptmonitoring::setFtdiBtn(QString mac){
    ftdi_btn = mac;
}

QString jptmonitoring::getFtdiBtn(){
    return ftdi_btn;
}

void jptmonitoring::recepcionMacFtdi(QString macFtdi){
    setFtdiBtn(macFtdi);
    writeTxtFTDI(macFtdi);
    //paso esta informacion a mi clase jptserialbtn
    a_serial_device_btn->set_radio_factory_and_baud(getFtdiBtn(), gv_data_configu[0][def_pos_conf_db_baud_rate].toInt());
    //guardamos la variable en el sistema local



    //cierra el metodo
}

void jptmonitoring::validarTxtFTDI(){
    //Se agrega nombre de archivo ftdi
    QString name_txt="ftdi_btn.txt";
    //Se carga el path de conexion o almacenmiento del archivo .txt
    QString path_source ="/jpt/jptmonitoring/External/"+ name_txt;

    QFile file(path_source);//leo el archivo si existe
    if(!file.exists()){
        //Si no existe se crea el archivo
        qDebug() << "EFECTIVO NO EXISTE";
        QFile archivo("/jpt/jptmonitoring/External/"+ name_txt);
        if(archivo.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            QTextStream datosArchivo(&archivo);
            datosArchivo <<"default"<<endl;
            qDebug()<<"ARCHIVO CREADO POR QUE NO EXISTE";
        }
        setFtdiBtn("default");
        archivo.close();
    }else{
        QString line("");
        if(file.open(QIODevice::ReadOnly)) {
            QTextStream in(&file);
            while(!in.atEnd()){
                line = in.readLine();
                setFtdiBtn(line);
                qDebug()<<line;
            }
        }
    }
    file.close();
}

void jptmonitoring::writeTxtFTDI(QString mac){
    QString name_txt="ftdi_btn.txt";
    QString path_source ="/jpt/jptmonitoring/External/"+ name_txt;
    QFile file(path_source);//leo el archivo si existe
    qDebug()<<"Existe.....";
    if(file.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text)){
        QTextStream datosArchivo(&file);
        //datosArchivo <<"";
        datosArchivo <<mac<<endl;
    }
    file.close();
    //qDebug()<<macFtdi;
    //setFtdiBtn(macFtdi);
}

void jptmonitoring::activeSirenFtdi(){


    QString signal_active="alarma_num_1";
    qDebug()<<"Activacion de sirenas";
    emit viewer_siren("Inicio Activacion: Boton Base");
    emit send_message_to_widget_v22(signal_active);
}

