#ifndef _SENTENCE__H
#define _SENTENCE__H
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
// [lv_ variables locales] [a_ atributos clase] [gv_ variables globales] [f_ variables de ciclos] [gui_ variables para interfaces]
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Explicacion cada sentencia del pre-procesador
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// [_HUB_ON             ]: Permite que se compile el programa para la BBB, con esto se busca que lea la base de datos desde la ubicacion en la BBB, ademas, conecta con los botones y leds
// [_FRAG_BITS_NAME_NODE]: Permite que la trama proveniente de los sensores se fragmente en 4{node|ID|value1|value2} y no en tres {node|value1|value2}
//
// [_COMPILE_GUI        ]: Permite que la aplicacion sea compilada con la interfaz grafica.
// [_COMPILE_MESSAGE    ]: Permite que se compilen los mensajes que
//
// [_TRAME_CLOUD_OLD    ]: Construye las tramas a la nube con la version vieja o REV3 en el programa Listener {python}
// [_TRAME_CLOUD_JSON   ]: Construye las tramas a la nube con la version json o REV2 en el prograam Listener {python}
//
// [_PROVE_EQUATION     ]: Genera el codigo que permite verificar la escritura de las ecuaciones
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
//#define _HUB_ON
#define _RSB_ON
#define _PROVE_EQUATION
#define _COMPILE_GUI
#define _COMPILE_MESSAGE
#define _TRAME_CLOUD_OLD
//#define _FRAG_BITS_NAME_NODE
#define _CLK_UPDATE

//--------------------------- PARA BLINK -----------------------------------------------------------------------------------------
//[_BLINK_CLOUD         ]:
//[_BLINK_SERIAL        ]:
//
//[_BLINK_MODBUS        ]:
//[_BLINK_420           ]:
//
//
// 1. Modo de datos
// [num]        [modo]              [hace]
// 66           constante           conectado el radio
//              blink               recepcion datos
// 67           constante           conectado a internet
//              blink               envio datos a la nube correctamente
// 68           constante[boton]    copiando informacion a la memoria
//              blink               transmitio datos por modbus - 4-20
// 70           Reiniciar la beagle cuando no se encuentre conectado el internet.
// 71           Reiniciar la beagle cuando el programa se bloquee.
//--------------------------------------------------------------------------------------------------------------------------------
#define _BLINK_CLOUD
#define _BLINK_SERIAL
//#define _BLINK_MODBUS
#define _BLINK_420

#define def_led_serial_db_on    "sudo sh -c 'echo 1 > /sys/class/gpio/gpio66/value'"
#define def_led_serial_db_off   "sudo sh -c 'echo 0 > /sys/class/gpio/gpio66/value'"

#define def_led_export_db_on    "sudo sh -c 'echo 1 > /sys/class/gpio/gpio68/value'"
#define def_led_export_db_off   "sudo sh -c 'echo 0 > /sys/class/gpio/gpio68/value'"

#define def_led_modbus_db_on def_led_export_db_on
#define def_led_modbus_db_off def_led_export_db_off

#define def_led_420_db_on def_led_export_db_on
#define def_led_420_db_off def_led_export_db_off

#define def_led_cloud_db_on     "sudo sh -c 'echo 1 > /sys/class/gpio/gpio67/value'"
#define def_led_cloud_db_off    "sudo sh -c 'echo 0 > /sys/class/gpio/gpio67/value'"

#define def_pin_wd_eth_on       "sudo sh -c 'echo 1 > /sys/class/gpio/gpio69/value'"
#define def_pin_wd_eth_off      "sudo sh -c 'echo 0 > /sys/class/gpio/gpio69/value'"

//--------------------------- PARA MODBUS ----------------------------------------------------------------------------------------
// 2. Codigo implementado para Inputs Register
//--------------------------------------------------------------------------------------------------------------------------------
#define _SET_INPUTS_REGISTER
#define _SET_INPUTS_DISCRETE

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
// Logica de sentencias
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
#ifndef _TRAME_CLOUD_OLD
    #define _TRAME_CLOUD_JSON
#endif

#endif // _SENTENCE__H
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->

