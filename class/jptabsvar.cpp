#include "jptabsvar.h"

jptabsvar::jptabsvar(QString name_node) : a_name_node(name_node){}
jptabsvar::jptabsvar(){}

//******************************************************************************************************************************
// Setter de las funciones de cada sensor                                                                            [jptabsvar]
//******************************************************************************************************************************
void jptabsvar::set_all_info(QString sensor_function, QString master_function, QString correc_function, QString name_var, int num_sen){
    if(num_sen == def_sensor_1_thr){
        a_functions_var_1[def_pos_abs_function_s]  = sensor_function;
        a_functions_var_1[def_pos_abs_function_m]  = master_function;
        a_functions_var_1[def_pos_abs_function_c]  = correc_function;
        a_functions_var_1[def_pos_abs_name_sensor] = name_var;
    }else{
        a_functions_var_2[def_pos_abs_function_s]  = sensor_function;
        a_functions_var_2[def_pos_abs_function_m]  = master_function;
        a_functions_var_2[def_pos_abs_function_c]  = correc_function;
        a_functions_var_2[def_pos_abs_name_sensor] = name_var;
    }
}


#ifdef _COMPILE_GUI
void jptabsvar::set_sensor_calib(QString sensor_function, int num_sen){
    if(num_sen == def_sensor_1_thr)
        a_functions_var_1[def_pos_abs_function_s] = sensor_function;
    else
        a_functions_var_1[def_pos_abs_function_s] = sensor_function;
}

void jptabsvar::set_master_calib(QString master_function, int num_sen){
    if(num_sen == def_sensor_1_thr)
        a_functions_var_1[def_pos_abs_function_m] = master_function;
    else
        a_functions_var_2[def_pos_abs_function_m] = master_function;
}

void jptabsvar::set_correc_calib(QString correc_function, int num_sen){
    if(num_sen == def_sensor_1_thr)
        a_functions_var_1[def_pos_abs_function_c] = correc_function;
    else
        a_functions_var_2[def_pos_abs_function_c] = correc_function;
}
#endif
//******************************************************************************************************************************
// Funcion                                                                                                           [jptabsvar]
//******************************************************************************************************************************
void jptabsvar::run(){
    if(a_functions_var_1[def_pos_abs_name_sensor] != "null" || a_functions_var_2[def_pos_abs_name_sensor] != "null"){
        QString value_s1("0");
        QString value_s2("0");
        if(a_functions_var_1[def_pos_abs_name_sensor] != "null"){
            value_s1 = calculate_functions(def_sensor_1_thr);
#ifdef _COMPILE_GUI
            emit view_value_calib_s1(value_s1);
#endif
        }
        if(a_functions_var_2[def_pos_abs_name_sensor] != "null"){
            value_s2 = calculate_functions(def_sensor_2_thr);
#ifdef _COMPILE_GUI
            emit view_value_calib_s2(value_s2);
#endif
        }
        emit send_to(value_s1, value_s2, a_name_node);
    }
}

//******************************************************************************************************************************
// Funcion que calcula el valor de ingenieria, pasado por las ecuaciones                                             [jptabsvar]
//******************************************************************************************************************************
QString jptabsvar::calculate_functions(const int num_sen){
    double  lv_value;
    QString error("C");
    try{
        calculate_function_sensor(&lv_value, num_sen);
        try{
            calculate_function_master(&lv_value, num_sen);
            try{
                calculate_function_correc(&lv_value, num_sen);
            }catch(...){
                error = "E.F.C";
            }
        }catch(...){
            error = "E.F.M";
        }
    }catch(...){
        error = "E.F.S";
    }

    if(error  == "C")
        error = QString::number(lv_value);
    return error;
}
//******************************************************************************************************************************
// Funcion  calcula con la ecuacion del sensor a_function[0]                                                         [jptabsvar]
//******************************************************************************************************************************
void jptabsvar::calculate_function_sensor(double *value_init, int num_sen){
    QString lv_temp_function;
    if(num_sen == def_sensor_1_thr){
        lv_temp_function = a_functions_var_1[def_pos_abs_function_s];
        lv_temp_function.replace("w", a_raw_sen_1);
    }
    else{
        lv_temp_function = a_functions_var_2[def_pos_abs_function_s];
        lv_temp_function.replace("w", a_raw_sen_2);
    }

    ValueList       lv_vlist;
    FunctionList    lv_flist;
    lv_vlist.AddDefaultValues();
    lv_flist.AddDefaultFunctions();

    Expression lv_e;
    lv_e.SetValueList(&lv_vlist);
    lv_e.SetFunctionList(&lv_flist);
    lv_e.Parse(lv_temp_function.toStdString());

    *value_init = lv_e.Evaluate();
}
//******************************************************************************************************************************
// Funcion  calcula con la ecuacion del master a_function[1]                                                         [jptabsvar]
//******************************************************************************************************************************
void jptabsvar::calculate_function_master(double *value_sensor, int num_sen){
    QString lv_temp_function;
    if(num_sen == def_sensor_1_thr)
        lv_temp_function = a_functions_var_1[def_pos_abs_function_m];
    else
        lv_temp_function = a_functions_var_2[def_pos_abs_function_m];
    lv_temp_function.replace("w", QString::number(*value_sensor));

    ValueList       lv_vlist;
    FunctionList    lv_flist;
    lv_vlist.AddDefaultValues();
    lv_flist.AddDefaultFunctions();

    Expression lv_e;
    lv_e.SetValueList(&lv_vlist);
    lv_e.SetFunctionList(&lv_flist);
    lv_e.Parse(lv_temp_function.toStdString());

    *value_sensor = lv_e.Evaluate();
}
//******************************************************************************************************************************
// Funcion  calcula con la ecuacion del correccion a_function[2]                                                     [jptabsvar]
//******************************************************************************************************************************
void jptabsvar::calculate_function_correc(double *value_master, int num_sen){
    QString lv_temp_function;
    if(num_sen == def_sensor_1_thr)
        lv_temp_function = a_functions_var_1[def_pos_abs_function_c];
    else
        lv_temp_function = a_functions_var_2[def_pos_abs_function_c];
    lv_temp_function.replace("w", QString::number(*value_master));

    ValueList       lv_vlist;
    FunctionList    lv_flist;
    lv_vlist.AddDefaultValues();
    lv_flist.AddDefaultFunctions();

    Expression lv_e;
    lv_e.SetValueList(&lv_vlist);
    lv_e.SetFunctionList(&lv_flist);
    lv_e.Parse(lv_temp_function.toStdString());

    *value_master = lv_e.Evaluate();
}
