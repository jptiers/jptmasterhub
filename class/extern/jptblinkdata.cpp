#include "jptblinkdata.h"

#define def_time_delay_leds 50

jptblinkdata::jptblinkdata(int type){
    this->a_type = type;
}

void jptblinkdata::run(){
    switch (a_type) {
        case def_modbus_data:
            blink_modbus();
        break;
        case def_serial_data:
            blink_serial();
        break;
        case def_cloud_data:
            blink_cloud();
        break;
        case def_420_data:
            blink_420();
        break;
    }
}

void jptblinkdata::blink_modbus(){
#ifdef _HUB_ON
    system(def_led_modbus_db_on);
    msleep(def_time_delay_leds);
    system(def_led_modbus_db_off);
    msleep(def_time_delay_leds );
    system(def_led_modbus_db_on);
    msleep(def_time_delay_leds);
    system(def_led_modbus_db_off);
#endif
    jptblinkdata::exit();
}

void jptblinkdata::blink_420(){
#ifdef _HUB_ON
    system(def_led_420_db_on);
    msleep(def_time_delay_leds);
    system(def_led_420_db_off);
    msleep(def_time_delay_leds);
    system(def_led_420_db_on);
    msleep(def_time_delay_leds);
    system(def_led_420_db_off);
#endif
    jptblinkdata::exit();
}

void jptblinkdata::blink_cloud(){
#ifdef _HUB_ON
    system(def_led_cloud_db_on);
    msleep(def_time_delay_leds);
    system(def_led_cloud_db_off);
    msleep(def_time_delay_leds);
    system(def_led_cloud_db_on);
    msleep(def_time_delay_leds);
    system(def_led_cloud_db_off);
    sleep(def_time_delay_leds);
    system(def_led_cloud_db_on);
#endif
    jptblinkdata::exit();
}

void jptblinkdata::blink_serial(){
#ifdef _HUB_ON
    system(def_led_serial_db_on);
    msleep(def_time_delay_leds);
    system(def_led_serial_db_off);
    msleep(def_time_delay_leds);
    system(def_led_serial_db_on);
    msleep(def_time_delay_leds);
    system(def_led_serial_db_off);
    msleep(def_time_delay_leds);
    system(def_led_serial_db_on);
#endif
    jptblinkdata::exit();
}
