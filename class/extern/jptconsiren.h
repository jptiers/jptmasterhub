#ifndef JPTCONSIREN_H
#define JPTCONSIREN_H

#include <QThread>
#include <QProcess>

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QJsonDocument>
#include <QJsonObject>

#include "_sentence_.h"

class jptconsiren : public QThread{
    Q_OBJECT

private:
    void status_siren(QProcess *ethernet);

    void restart_network();
    QByteArray ip;
    QVector<bool>         gv_status_clients;
    QStringList           gv_name_widget;

    QStringList           my_sirens;
public:
    jptconsiren();
    void set_values(QVector<bool> status_clients, QStringList name_widget);
    void set_sirens(QStringList sirens_list);
    void run();

signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif
    void disconect_siren(int client_id,QString client_ip);

};


#endif // jptconsiren_H
