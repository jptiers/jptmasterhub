#include "jptconsiren.h"


#define def_delay_scan_eth0     40000
#define def_lim_counter_restart 30
#define def_lim_counter_etho    10

int gv_count_error_ping_siren(0);


jptconsiren::jptconsiren(){}

void jptconsiren::set_values(QVector<bool> status_clients,QStringList name_widget)
{
    //estado de entrada delos valores
    qDebug()<<"NUEVA SIRENA REGISTRADA";
    gv_status_clients.clear();
    gv_status_clients=status_clients;

    gv_name_widget.clear();
    gv_name_widget=name_widget;

}

void jptconsiren::set_sirens(QStringList sirens_list)
{
    my_sirens=sirens_list;
}
//******************************************************************************************************************************
// Verifica que haya conexion a los widgets                                                                       [jptconsiren]
//******************************************************************************************************************************
void jptconsiren::run(){
    qDebug()<<"Start a_watch_consirens";
    QProcess lv_widget;
    for(;;)
        status_siren(&lv_widget);
}

//******************************************************************************************************************************
// Verifica el estado del internet mientras realiza un Ping a cada elemento
//******************************************************************************************************************************
void jptconsiren::status_siren(QProcess *widget){
    //se evalua que existan widgets conectados
    qDebug()<<gv_name_widget.size();
    if(gv_name_widget.size()!=0)
    {    qDebug()<<"SIRENAS CONECTADAS ---------------------------------------------------------------------------------------------------------------";

        for(int i=(gv_name_widget.size()-1);i>-1;i--)
        {  QStringList parse=gv_name_widget[i].split("||");
           QString _ip=parse[1];
           //qDebug()<<"nombre del equipo:"<<gv_name_widget[i];
           //qDebug()<<"ip del equipo"<<_ip;

           //ya con la ip seprocede a realizar la consulta del ping a la sirena
           QString result = "";
           widget= new QProcess();
           widget->start("ping "+_ip+" -c 1");
           widget->waitForFinished(-1);
           result  = widget->readAll();
           widget->close();

           if(result.contains("1 received") || result.contains("1 recibido"))
           {


                qDebug()<<"ip:"+_ip+"||Actualmente conectada>>"+QString::number(i);
           }
           else
           {
                qDebug()<<"ip:"+_ip+"||Actualmente Desconectada>>"+QString::number(i);
                //cuando se desconecta debemos enviar una señal de desconexion.
                emit disconect_siren(i,_ip);

           }


        }
         qDebug()<<"FINAL ROW ---------------------------------------------------------------------------------------------------------------";
    }
    else{

         qDebug()<<"Ninguna Sirena Conectada ---------------------------------------------------------------------------------------------------------------";
    }


    msleep(def_delay_scan_eth0);
}

