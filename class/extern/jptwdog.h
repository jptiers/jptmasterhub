#ifndef JPTWDOG_H
#define JPTWDOG_H

#include <QThread>
#include <_sentence_.h>

class jptwdog : public QThread{
    Q_OBJECT

public:
    jptwdog();
    void run();

private:
    void pwm_pin();

public slots:
    void reset_network();

};

#endif // JPTWDOG_H
