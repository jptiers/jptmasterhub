#include "jptexportdb.h"

#define def_delay_on_export      2000

const QByteArray gv_name_data_base("jptdatabase.db");

jptexportdb::jptexportdb(QString a_name_hub) : name_hub(a_name_hub){}

//******************************************************************************************************************************
// Exporta la base de datos a la memoria llamada JPT
//******************************************************************************************************************************
void jptexportdb::run(){
#ifdef _HUB_ON
    system(def_led_export_db_on);
    QByteArray name_data_base(name_hub.toLatin1() + "_" + gv_name_data_base);

    //system("sudo sh -c 'chattr -i /media/JPTSD/.JPT/" + gv_name_data_base + "'");

    system("sudo chmod 777 /media/JPTSD/.JPT/" + gv_name_data_base);
    system("sudo cp /media/JPTSD/.JPT/" + gv_name_data_base + " /media/JPT/" + name_data_base);
    system("sudo chmod 777 /media/JPTSD/.JPT/" + gv_name_data_base);

    //system("sudo sh -c 'chattr +i /media/JPTSD/.JPT/" + gv_name_data_base + "'");
#endif
    msleep(def_delay_on_export);
#ifdef _HUB_ON
    system(def_led_export_db_off);
#endif
#ifdef _COMPILE_MESSAGE
    emit viewer_status("[o] Export DB");
#endif
    jptexportdb::exit();
}
