#include "jptbtnexport.h"

#define def_read_btn_export "cat /sys/class/gpio/gpio31/value"
#define def_delay_scan_btn 3000

jptbtnexport::jptbtnexport(){}

//******************************************************************************************************************************
// Verifica que el boton(externo) de exportar la base de datos a la memoria este presionado                       [jptbtnexport]
//******************************************************************************************************************************
void jptbtnexport::run(){
    for(;;){
        msleep(def_delay_scan_btn);

        QProcess reader;
        reader.start(def_read_btn_export);
        reader.waitForFinished(-1);

        int lv_status_btn_export = reader.readAll().mid(0, 1).toInt();
        reader.close();

        if ((!lv_status_btn_export) && (!a_btn_status_export)){
            emit export_db_memory();
            a_btn_status_export = def_status_exporting;
        }else if ((lv_status_btn_export) && (a_btn_status_export))
            a_btn_status_export = def_status_none;
    }
    jptbtnexport::exit();
}
