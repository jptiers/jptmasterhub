#include "jptethernet.h"

#define def_delay_scan_eth0     6000
#define def_lim_counter_restart 30
#define def_lim_counter_etho    10

int gv_count_error_ping(0);

jptethernet::jptethernet(){}
//******************************************************************************************************************************
// Verifica que haya conexion a internet                                                                           [jptethernet]
//******************************************************************************************************************************
void jptethernet::run(){
    QProcess lv_ethernet;
    for(;;)
        status_ethernet(&lv_ethernet);
}

//******************************************************************************************************************************
// Verifica el estado del internet mientras realiza un Pin
//******************************************************************************************************************************
void jptethernet::status_ethernet(QProcess *ethernet){
    QString result = "";
    ethernet->start("ping www.safewirelessmonitor.com -c 1");
    ethernet->waitForFinished(-1);
    result  = ethernet->readAll();
    ethernet->close();

    if(result.contains("1 received") || result.contains("1 recibido")){
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[o] Ethernet");
#endif
        emit status_ethernet(true);
        gv_count_error_ping = 0;
#ifdef _HUB_ON
        system(def_led_cloud_db_on);
#endif
    }else{
        
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[*] Ethernet");
#endif
        emit status_ethernet(false);
        ++gv_count_error_ping;


        if(gv_count_error_ping == def_lim_counter_etho || gv_count_error_ping == def_lim_counter_etho + 10)
            restart_network();

        if(gv_count_error_ping > def_lim_counter_restart)
            emit restart_beagle_signal();
#ifdef _HUB_ON
        system(def_led_cloud_db_off);
#endif
    }
    msleep(def_delay_scan_eth0);
}

#ifdef _HUB_ON
void jptethernet::restart_beagle(){
#ifdef _COMPILE_MESSAGE
    emit viewer_status("[!] reset BBB");
#endif
    //system(def_pin_wd_eth_on);
    //msleep(30);
    //system(def_pin_wd_eth_off);
}
#endif


void jptethernet::restart_network(){
#ifdef _HUB_ON
    system("sudo sh -c 'python /jpt/jptmonitoring/ethupdate.pyc'");
#endif
#ifdef _COMPILE_MESSAGE
    emit viewer_status("[!] Reset network");
#endif
}

