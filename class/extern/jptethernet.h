#ifndef JPTETHERNET_H
#define JPTETHERNET_H

#include <QThread>
#include <QProcess>
#include "_sentence_.h"

class jptethernet : public QThread{
    Q_OBJECT

private:
    void status_ethernet(QProcess *ethernet);

    void restart_network();
#ifdef _HUB_ON
    void restart_beagle();
#endif

    QByteArray ip;
public:
    jptethernet();
    void run();

signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif
    void status_ethernet(bool status);
    void restart_beagle_signal();
};


#endif // JPTETHERNET_H
