#ifndef JPTABSVAR_H
#define JPTABSVAR_H

#include <QThread>
#include <QString>
#include <cstdio>
#include <string>
#include "_sentence_.h"

#include <class/expression/expreval.h>

#define def_pos_abs_function_s      0
#define def_pos_abs_function_m      1
#define def_pos_abs_function_c      2
#define def_pos_abs_name_sensor     3

#define def_sensor_1_thr    1
#define def_sensor_2_thr    2

using namespace ExprEval;
using namespace std;

class jptabsvar : public QThread
{
    Q_OBJECT

public:
    jptabsvar(QString name_node);
    jptabsvar();

    void run();


    void set_all_info(QString sensor_function, QString master_function, QString correc_function, QString name_var, int num_var);
#ifdef _COMPILE_GUI
    void set_sensor_calib(QString sensor_function, int num_var);
    void set_master_calib(QString master_function, int num_var);
    void set_correc_calib(QString correc_function, int num_var);
#endif

    QString a_raw_sen_1;
    QString a_raw_sen_2;

private:
    void calculate_function_sensor(double *value_init, int num_sen);
    void calculate_function_master(double *value_sensor, int num_sen);
    void calculate_function_correc(double *value_master, int num_sen);

    QString a_functions_var_1[4]; // [0] Sensor function, [1] master function, [2] correction function, [3] name variable
    QString a_functions_var_2[4];
    QString a_name_node;

    QString calculate_functions(const int num_sen);

signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString);
#endif
#ifdef _COMPILE_GUI
    void view_value_calib_s1(QString);
    void view_value_calib_s2(QString);
#endif
    void send_to(QString value_s1, QString value_s2, QString name_node);

};

#endif // JPTABSVAR_H
