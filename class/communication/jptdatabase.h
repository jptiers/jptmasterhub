#ifndef JPTDATABASE_H
#define JPTDATABASE_H

#include <QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>

#include "_sentence_.h"

#define def_pos_table_func  0
#define def_pos_table_conf  1
#define def_pos_table_char  2
#define def_pos_table_modu  3
#define def_pos_table_pwms  4
#define def_pos_table_dela  5

#define def_num_colums_table_func 10
#define def_num_colums_table_conf 25
#define def_num_colums_table_char 23 // +2 fact (Ya sumados)
#define def_num_colums_table_modu 2
#define def_num_colums_table_pwms 9
#define def_num_colums_table_dela 4

class jptdatabase : public QObject{
    Q_OBJECT
public:
    explicit jptdatabase(QString name_data_base, QString password, QString user_name, QObject *parent = nullptr);
    ~jptdatabase();
    void update_register(QString name_table, QString table_values);
    void update_register(QString name_table, QString id, QString query);

    void get_values      (int pos_name_table, QVector<QStringList > *vec_data);
    void init_database_connection();


    void update_port_cloud(QString port_cloud);
    void update_ip_cloud(QString ip_cloud);
    void update_hub_name(QString hub_name);

public slots:
    void insert_register (QString name_table, QString table_values);
    void update_level();
    QStringList get_data_config();
    void set_data_config(int id,QString dato);
    QString table_from_device(QString device, int num_sensor,QList<double> list_ing_data);


signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif

private:
    QSqlDatabase db;


};

#endif // JPTDATABASE_H
