#include "jptconwidgets.h"
#include <QFile>
#include <QAudioFormat>
#include <QAudioInput>
#include <QObject>
#include <QDataStream>
#include <QImage>
#include <QBuffer>



#define def_status_client_not_identifity false
#define def_status_client_yes_identifity true

QVector<QTcpSocket *> gv_clients;
QVector<bool>         gv_status_clients;
QStringList           gv_name_widget;
bool gv_conection     = true;


//******************************************************************************************************************************
// Funcion principal                                                                                             [jptconwidgets]
//******************************************************************************************************************************
jptconwidgets::jptconwidgets(QObject *parent) : QObject(parent){
    a_server_wd = new QTcpServer(this);
    a_server_wd->listen(QHostAddress(def_ip_widgets_communication), def_port_widgets_communication);
    a_server_wd->setMaxPendingConnections(def_num_max_connections);

    connect(a_server_wd, SIGNAL(newConnection()), this, SLOT(new_connection()));

    //a_watch_status_sirens = new jptconsiren();
    //connect(a_watch_status_sirens, SIGNAL(disconect_siren(int,QString)), this, SLOT(disconect_siren(int,QString)));


}
//******************************************************************************************************************************
// Escucha una nueva coneccion presente                                                  SIGNAL[QTcpServer:newConnection - SLOT]
//******************************************************************************************************************************
void jptconwidgets::new_connection(){
    if(gv_conection)
    {
        qDebug()<<gv_clients.size()<<"Sirenas antes de conexion";
        gv_clients.append(a_server_wd->nextPendingConnection());
        qDebug()<<gv_clients.size()<<"Despues de conexion";
        gv_status_clients.append(def_status_client_not_identifity);
        int lv_pos_client = gv_clients.size() - 1;

        qDebug()<<"Thats My Lv Vector position<<"<<lv_pos_client;


        connect(gv_clients[lv_pos_client], SIGNAL(readyRead()), this, SLOT(message_from_widget()));
        connect(gv_clients[lv_pos_client], SIGNAL(disconnected()), this, SLOT(disconnected()));
        gv_conection = false;


    }
    else
    {
        QTcpSocket *socket = a_server_wd->nextPendingConnection();
        socket->close();
        socket->deleteLater();
    }
}

//******************************************************************************************************************************
// Escucha cuando una conexion ha sido cerrada pormedio                                             SIGNAL[QTcpSocket:disconnected- SLOT]
//******************************************************************************************************************************
void jptconwidgets::disconect_siren(int client_id,QString client_ip){
    qDebug()<<"Disconnecting Siren :" + client_ip;
    emit viewer_status("[*] Widget: " + gv_name_widget[client_id]);//se emite señal de desconexion
    int position=1000;
    for (int i=0;i<ip_client_list.size();i++)
    {
        if(ip_client_list[i]==client_ip)
        {
            position=i;//guardola poicion del vector

        }

    }
    if(position!=1000)//verifico que si cambio
    {
        status_client_list[position]=false;
        emit status_conection_widget(status_client_list);
    }


    gv_clients.remove(client_id);
    gv_status_clients.remove(client_id);
    gv_name_widget.removeAt(client_id);
   // a_watch_status_sirens->set_values(gv_status_clients,gv_name_widget);
}

//******************************************************************************************************************************
// Escucha cuando una conexion ha sido cerrada                                             SIGNAL[QTcpSocket:disconnected- SLOT]
//******************************************************************************************************************************
void jptconwidgets::disconnected(){
    for (int f_num_client = 0; f_num_client < gv_clients.size(); ++f_num_client)
        if(gv_clients[f_num_client]->state() == QAbstractSocket::UnconnectedState){

            emit status_conection_widget(status_client_list);
#ifdef _COMPILE_MESSAGE
            emit viewer_status("[*] Widget: " + gv_name_widget[f_num_client]);
#endif
            QString nombre_parse=gv_name_widget[f_num_client];//obtengo el nombre a parsear
            QStringList data_parse=nombre_parse.split("||");
            QString data=data_parse[1];
            ////qDebug()<<data<<"Este elemento se desconecto";
            int position=1000;
            for (int i=0;i<ip_client_list.size();i++)
            {
                if(ip_client_list[i]==data)
                {
                    position=i;//guardola poicion del vector

                }

            }
            if(position!=1000)//verifico que si cambio
            {
                status_client_list[position]=false;
                emit status_conection_widget(status_client_list);
            }


            gv_clients.remove(f_num_client);
            gv_status_clients.remove(f_num_client);
            gv_name_widget.removeAt(f_num_client);
        }
}



//******************************************************************************************************************************
// Transmite los valores crudos                                           SIGNAL[jptmonitoring:send_message_to_widget - SLOT]
//******************************************************************************************************************************
void jptconwidgets::send_message_to_widget_raw(QString name_node, QString value_1, QString value_2){
    //El sistema funciona como n broadcast disparando la misma señal a todos los elementos de la red.
    for(int f_num_client = 0; f_num_client < gv_clients.size(); ++f_num_client)
            if(gv_status_clients[f_num_client])
                if(gv_name_widget[f_num_client] == "Viewer")
                {
                    QString lv_message_widget = "raw-" + name_node + "-" + value_1 + "-" + value_2 + "-##";
                    gv_clients[f_num_client]->write(lv_message_widget.toLatin1());
                    gv_clients[f_num_client]->waitForBytesWritten(-1);
    #ifdef _COMPILE_MESSAGE
                    emit viewer_status("[>] Message to " + gv_name_widget[f_num_client]);
    #endif
            }



}
//******************************************************************************************************************************
// Transmite los datos al widget                                             SIGNAL[jptmonitoring:send_message_to_widget - SLOT]
//******************************************************************************************************************************
// En el instalador definir para los nodos y decir para que wiget van, con un QCombobox que defina el widget, con lo cual se
// enviara el nombre del widget al cual esta asignado ese nodo en especifico, y en la trama que contiene nombre de los sensores
// emisores de los datos; seleccionar esa tarjeta para el widget destino.
//******************************************************************************************************************************
void jptconwidgets::send_message_to_widget(QStringList name_widget, QString name_node, QString value_1, QString value_2){
    QString lv_message_widget;

    for(int f_num_client = 0; f_num_client < gv_clients.size(); ++f_num_client)
        if(gv_status_clients[f_num_client]){
            if(gv_name_widget[f_num_client] == "Viewer")
                lv_message_widget = "ing-" + name_node + "-" + value_1 + "-" + value_2 + "-##";
            else
                lv_message_widget = name_node + "-" + value_1 + "-" + value_2 + "-##";
            foreach(const QString f_name_widget, name_widget)
                if(gv_name_widget[f_num_client] == f_name_widget){                    
                    gv_clients[f_num_client]->write(lv_message_widget.toLatin1());
                    gv_clients[f_num_client]->waitForBytesWritten(-1);
    #ifdef _COMPILE_MESSAGE
                    emit viewer_status("[>] Message to " + gv_name_widget[f_num_client]);
    #endif
                }
        }

}

void jptconwidgets::send_message_to_widget_v2(QString name_node)
{   ////qDebug()<<"proceso de envio de señal ";
    for(int f_num_client = 0; f_num_client < gv_clients.size(); ++f_num_client)
    {
        if(gv_status_clients[f_num_client] && name_node!="Stop" && name_node!="Broadcast_Start" && name_node!="Broadcast_Stop"  )
        {
            QStringList name=gv_name_widget[f_num_client].split("_");
            ////qDebug()<<name[0];
            if(name[0] == "Siren" || name[0] == "siren")//valido si el nombre es un tipo de sirena
            {
                QJsonObject lv_trame;
                ////qDebug()<<"los datos que me interesan son :"<<get_sound_time()<<get_repeat_times();
                QString data_to_send=name_node+"||"+QString::number(get_sound_time())+"||"+QString::number(get_repeat_times())+"||0";
                lv_trame["name"]    =data_to_send;
                lv_trame["mode"]    = "Sound";

                QJsonDocument lv_document(lv_trame);
                QString lv_str_trame(lv_document.toJson());

                gv_clients[f_num_client]->write(lv_str_trame.toLatin1().data());
                gv_clients[f_num_client]->waitForBytesWritten(-1);
                //en este punto ponga la validacion del sistema de datos.
                emit viewer_status("[>] Message send: Active sound.");
            }
        }
        else if(gv_status_clients[f_num_client] && name_node=="Stop")
        {
            QStringList name=gv_name_widget[f_num_client].split("_");
            ////qDebug()<<name[0];
            if(name[0] == "Siren" || name[0] == "siren")//valido si el nombre es un tipo de sirena
            {
                QJsonObject lv_trame;

                lv_trame["name"]    ="Stop";
                lv_trame["mode"]    ="Stop";

                QJsonDocument lv_document(lv_trame);
                QString lv_str_trame(lv_document.toJson());

                gv_clients[f_num_client]->write(lv_str_trame.toLatin1().data());
                gv_clients[f_num_client]->waitForBytesWritten(-1);
                //en este punto ponga la validacion del sistema de datos.
                emit viewer_status("[>] Message send: Closed sound.");
            }


        }

        else if(gv_status_clients[f_num_client] && name_node=="Broadcast_Start")
        {
            QStringList name=gv_name_widget[f_num_client].split("_");
            ////qDebug()<<name[0];
            if(name[0] == "Siren" || name[0] == "siren")//valido si el nombre es un tipo de sirena
            {
                QJsonObject lv_trame;

                lv_trame["name"]    ="Broadcast_Start";
                lv_trame["mode"]    ="Broadcast_Start";

                QJsonDocument lv_document(lv_trame);
                QString lv_str_trame(lv_document.toJson());

                gv_clients[f_num_client]->write(lv_str_trame.toLatin1().data());
                gv_clients[f_num_client]->waitForBytesWritten(-1);
                //en este punto ponga la validacion del sistema de datos.
                emit viewer_status("[>] Message send: Active Broadcast");
            }


        }

        else if(gv_status_clients[f_num_client] && name_node=="Broadcast_Stop")
        {
            QStringList name=gv_name_widget[f_num_client].split("_");
            ////qDebug()<<name[0];
            if(name[0] == "Siren" || name[0] == "siren")//valido si el nombre es un tipo de sirena
            {
                QJsonObject lv_trame;

                lv_trame["name"]    ="Broadcast_Stop";
                lv_trame["mode"]    ="Broadcast_Stop";

                QJsonDocument lv_document(lv_trame);
                QString lv_str_trame(lv_document.toJson());

                gv_clients[f_num_client]->write(lv_str_trame.toLatin1().data());
                gv_clients[f_num_client]->waitForBytesWritten(-1);
                //en este punto ponga la validacion del sistema de datos.
                emit viewer_status("[>] Message send: Close Broadcast.");
            }


        }

    }
}
//******************************************************************************************************************************
// Recibe un mensaje del widget que se desea conectar                                                            [jptconwidgets]
//******************************************************************************************************************************
void jptconwidgets::message_from_widget(){
    ////qDebug()<<"message from siren";
    QString lv_message;
    for(int f_num_client = 0; f_num_client < gv_clients.size() ; ++f_num_client){
        if(gv_clients[f_num_client]->bytesAvailable() != 0){
            if(!gv_status_clients[f_num_client]){
                lv_message = gv_clients[f_num_client]->readAll();

                QJsonDocument lv_message_json = QJsonDocument::fromJson(lv_message.toUtf8());
                QJsonObject   lv_trame_json   = lv_message_json.object();

                if(lv_trame_json["mode"].toString() == "Start")
                {

                    //En este punto verifico si existe una ip con el mismo valor que la que se esta conectando
                    //obtengo una lista temporal con los valores de las ip
                    QStringList temp_ip_list=get_ip_clients();
                    //bandera de verificacion
                    bool bander_v=false;
                    int valor_posicion;

                    for (int elemento_lista=0;elemento_lista<temp_ip_list.size();elemento_lista++)
                    {
                        if(lv_trame_json["name"].toString()==temp_ip_list[elemento_lista])
                        {
                            bander_v=true;
                            valor_posicion=elemento_lista;

                        }
                    }
                    ////qDebug()<<bander_v<<valor_posicion;


                    if(bander_v)
                    {   //con el valor de la posicion cambio el status de la sirena guardado
                        status_client_list[valor_posicion]=true;
                        //proceso de inicio de mensaje, se debe recibir la ip y verificar en el sistema si existe
                        QStringList temp_name_list=get_name_clients();
                        gv_name_widget.append(temp_name_list[valor_posicion]+"||"+temp_ip_list[valor_posicion]);
                        //emito un msg a la consola
                        emit viewer_status("[o] Widget: " + gv_name_widget[f_num_client]);
                        //emito un mensaje para el cambio de estado de conexion
                        emit status_conection_widget(status_client_list);

                        QJsonObject lv_trame;
                        lv_trame["name"]    = temp_name_list[valor_posicion];
                        lv_trame["mode"]    = "Start";

                        QJsonDocument lv_document(lv_trame);
                        QString lv_str_trame(lv_document.toJson());

                        gv_clients[f_num_client]->write(lv_str_trame.toLatin1().data());
                        gv_clients[f_num_client]->waitForBytesWritten(-1);

                        gv_status_clients[f_num_client] = def_status_client_yes_identifity;
                        gv_conection = true;


                      //  a_watch_status_sirens->set_values(gv_status_clients,gv_name_widget);

                    }

                }



            }

            else{
                lv_message = gv_clients[f_num_client]->readAll();

                QJsonDocument lv_message_json = QJsonDocument::fromJson(lv_message.toUtf8());
                QJsonObject   lv_trame_json   = lv_message_json.object();

                if (lv_trame_json["mode"].toString() == "S_Connect") {

                    emit viewer_status(lv_trame_json["name"].toString()+"||Start Playing");

                }
                else if (lv_trame_json["mode"].toString() == "S_Process") {
                    emit viewer_status(lv_trame_json["name"].toString()+"||Playing...");

                }
                else if (lv_trame_json["mode"].toString() == "S_End") {
                    emit viewer_status(lv_trame_json["name"].toString()+"||Closed Playing");

                }
                else if (lv_trame_json["mode"].toString() == "bro_init") {
                    emit viewer_status(lv_trame_json["name"].toString()+"||BroadCast Start");

                }
                else if (lv_trame_json["mode"].toString() == "bro_end") {
                    emit viewer_status(lv_trame_json["name"].toString()+"||BroadCAst Closed");

                }

                else
                {
                      emit viewer_status("llego>>"+lv_message.toLatin1());

                }



            }
        }
    }
}
//******************************************************************************************************************************
//Consulta de las ip de los clientes permitidos
//******************************************************************************************************************************
void jptconwidgets::set_ip_clients(QStringList ip_list)
{
    ip_client_list.clear();
    ip_client_list=ip_list;

    //Luego de que se setean los valores de las ip, se procede a activar el sistema de jptconsiren
    ////qDebug()<<"inicio la lista de valores de conxion ";
  //  a_watch_status_sirens->set_sirens(ip_client_list);
   // a_watch_status_sirens->start();

}
//******************************************************************************************************************************
//Consulta de las ip de los clientes permitidos
//******************************************************************************************************************************
QStringList jptconwidgets::get_ip_clients()
{
    return ip_client_list;
}
//******************************************************************************************************************************
//Consulta de las ip de los clientes permitidos
//******************************************************************************************************************************
void jptconwidgets::set_name_clients(QStringList name_list)
{
    name_client_list.clear();
    name_client_list=name_list;

}
//******************************************************************************************************************************
//Consulta de las ip de los clientes permitidos
//******************************************************************************************************************************
QStringList jptconwidgets::get_name_clients()
{
    return name_client_list;

}

void jptconwidgets::set_status_client_list(QList<bool> status_list)
{
    status_client_list.clear();
    status_client_list=status_list;

}

QList<bool> jptconwidgets::get_status_clients_list()
{
    return status_client_list;
}

void jptconwidgets::set_send_client_list(QList<bool> send_list)
{
    send_client_list.clear();
    send_client_list=send_list;

}
QList<bool> jptconwidgets::get_send_client_list()
{
    return  send_client_list;
}

void jptconwidgets::set_sound_time(int _time)
{
   sound_time=_time;
}

int jptconwidgets::get_sound_time()
{
    return sound_time;
}

void jptconwidgets::set_repeat_times(int _times)
{
    repeat_time=_times;

}
int jptconwidgets::get_repeat_times()
{
    return repeat_time;
}













void jptconwidgets::change_status_send(QList<bool> data_entry)
{
    //actualizo mi lista
    send_client_list.clear();
    send_client_list=data_entry;
    /*for (int i=0;i<send_client_list.size();i++) {
        ////qDebug()<<send_client_list[i];
    }*/

}


