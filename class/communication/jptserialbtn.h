#ifndef JPTSERIALBTN_H
#define JPTSERIALBTN_H

#include "_sentence_.h"
#include <QTimer>
#include <QString>
#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <unistd.h>
#include <QQueue>
#include <class/extern/jptblinkdata.h>
#include <class/communication/jptdevices.h>

#define def_timer_delay_reconnect_serial    5000

#define def_bits_frag_trame                 4
#define def_bit_init_name_node              0

#ifdef _FRAG_BITS_NAME_NODE
    #define def_bit_init_name_hubs          2
    #define def_bits_frag_name_hub          2
#endif

#define def_bit_init_value_sen_1            4
#define def_bit_init_value_sen_2            8

class jptserialbtn : public QObject{
    Q_OBJECT

public:
    explicit jptserialbtn(QString serial_factory, int baud_rate, QObject *parent = 0);
    ~jptserialbtn();
#ifdef _COMPILE_GUI
    void set_radio_factory_and_baud(QString serial_factory, int baud_rate);
#endif

public slots:
    void read_serial_port();
    void serial_unplugged();
    void init_serial_connection();

    void parse_sensores(int id_equipo,int id_target,int num_sensores,QStringList _sensores,QString _random, QString check_sum);
    int get_int_from_string(QString _data);
    void get_trama_from_stack();
    void verificar_trama(QString _trame);


signals:
    void serial_data(QStringList);
    void send_trame_to_monitoring(int,int,int,QList<int>);
    void signalButttonFtdi();
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString);
#endif
    void status_serial(bool status, int id);

private:
    //-------------------------------------------------------------------------------------------------
    // Atributos de la clase
    //-------------------------------------------------------------------------------------------------
    QTimer      *a_serial_reconnect;
    QSerialPort *a_serial_port;
    QString      a_serial_factory;
    QString      a_serial_number;
    int          a_baud_rate;
    QString a_trame_cabecera;
    QString a_trame_cuerpo;
    QString a_trame_cola;

    QString a_trama_save;

    QQueue<QString> my_stack;//pila fifo para almancenamiento de los datos.
    int cabecera_num;
    bool cabecera_bool;
    int cola_num;

    jptdevices *device= new jptdevices();

    //-------------------------------------------------------------------------------------------------
    // @
    //-------------------------------------------------------------------------------------------------
    void init_timer_reconnect();
    void init_var(QString serial_factory);
    void trame_serial_decode(QString serial_trame);
    void try_connect_serial();

};

#endif // JPTSERIALBTN_H
