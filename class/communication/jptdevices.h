#ifndef JPTDEVICES_H
#define JPTDEVICES_H

#include <QObject>
#include <QThread>
#include <QString>
#include <class/expression/expreval.h>

using namespace ExprEval;
using namespace std;


class jptdevices : public QThread
{
    Q_OBJECT
public:

    //explicit jptdevices(QObject *parent = nullptr);
    jptdevices();

    void run();//clase principal del sistema.

    void init_device(int id_equipo,int id_targeta,int num_sensor,QList<int> raw_values);

    //funciones para obtener parametros del JSON
    int get_num_sensores(int equipo_id);//obtiene numero de sensores
    QString get_nombre_equipo(int equipo_id);//obtiene el nombre del equipo
    QStringList get_labels_sensores(int equipo_id);//obtiene los label de cada sensor

    QStringList get_transfer_f(int equipo_id,QString function);//obtiene las funciones de transferenca de cada sensor;
    //NOTA: las funciones que se puede obtner son T=transferencia , M=Maestra y C=Correcion
    //Funcones getter y setter necesarias
    int get_id_targeta();
    int get_id_device();
    int get_num_sensor_saved();

    QString get_nombre();   

    QList<int> get_raw_values();
    void set_sensor_raw(QList<int> values);

    QList<double> get_ing_values();
    void set_sensor_ing(QList<double> values);

    QStringList get_transfer_f();
    void set_transfer_f(QStringList funciones);

    QStringList get_master_f();
    void set_master_f(QStringList funciones);

    QStringList get_correct_f();
    void set_correct_f(QStringList funciones);

    QList<double> get_acumulate_f();
    void set_acumulate_f(QList<double> values);

    QStringList get_labels_saved();



    //funciones encargadas de generar la data de ingenieria .
    void txt_funcion_create(QString name_txt);
    QString calculate_functions();
    void calculate_function_sensor(QList<int> value_init);
    void calculate_function_master(QList<double> values_sensor);
    void calculate_function_correc(QList<double> value_master);

     void update_correct_f();


signals:
    void signal_to_viewer_reciver(QString,QString,QString,QString,QString,QString,QString,QString,int,QList<double>);
    void combo_box_element(QString);

public slots:

private:

    QString path;//ruta del archivo que contiene la nformacion de los equipos (Archivo: devices.json)
    QString path_devices;//ruta de la carpeta de almacenamiento de las funciones de tranferencia de los equipos.
    //valores que me entran a la funcion

    int id_device;//es el path que se buscara dentro del anterior archivo
    int id_target; //Auqnue no es neesario el sistema guardara el id de la targeta que se esta usando
    int num_sensores; //cantidad de sensores que se encuentran en el equipo
    QList<int> value_sensors_raw;//valores de los sensores de 0 a 16384
    QList<double> value_sensors_ing;
    QList<double> acumlator_functions;



    QString nombre;//nombre del equipo que se localizo para el id respectivo
    QStringList label_sensores;//listado de sensores que posee el equipo
    QStringList transfer_f;//Funciones de transferencia de los sensores del equipo.
    QStringList master_f;
    QStringList correct_f;

    bool is_level=false;
    bool first_row=true;
    double first_value=0;




};

#endif // JPTDEVICES_H
