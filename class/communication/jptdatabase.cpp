#include "jptdatabase.h"
#include <QtDebug>
#include <QDateTime>

const QString           gv_list_name_tables[6] = {"Functions","Configurations","Characteristics","Modules","PWM","Delayer"};
//*********************************************************************************************************************************************************
// Constructor
//*********************************************************************************************************************************************************
jptdatabase::jptdatabase(QString name_data_base, QString password, QString user_name, QObject *parent) : QObject(parent){

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/jpt/jptmonitoring/External/" + name_data_base);
    db.setPassword(password);
    db.setUserName(user_name);





}
//*********************************************************************************************************************************************************
// Inicializa la conexion con la base de datos
//*********************************************************************************************************************************************************
void jptdatabase::init_database_connection(){
#ifndef _COMPILE_MESSAGE
    db.open();
#else
    if(db.open())
        emit viewer_status("[o] Database");
    else
        emit viewer_status("[*] Database");


#endif
}
//*********************************************************************************************************************************************************
// Destructor
//*********************************************************************************************************************************************************
jptdatabase::~jptdatabase(){
    db.close();
}
//*********************************************************************************************************************************************************
// Insertar Datos en las tablas
//*********************************************************************************************************************************************************
void jptdatabase::insert_register(QString name_table, QString query){
    QString new_query;
    new_query.append("INSERT INTO " + name_table + query);
    //////qDebug()<<"QUERY TO SQLITE";
    //////qDebug()<<new_query;
    QSqlQuery create;
    create.prepare(new_query);

    create.exec();
}

//*********************************************************************************************************************************************************
// Obtener los registros necesitados
//*********************************************************************************************************************************************************
void jptdatabase::get_values(int pos_name_table, QVector<QStringList > *vec_data){
    vec_data->clear();

    QString lv_name_table(gv_list_name_tables[pos_name_table]);
    int lv_num_item_table(0);

         if(pos_name_table == def_pos_table_func)
             lv_num_item_table  = def_num_colums_table_func;
    else if(pos_name_table == def_pos_table_conf)
             lv_num_item_table  = def_num_colums_table_conf;
    else if(pos_name_table == def_pos_table_char)
             lv_num_item_table  = def_num_colums_table_char;
    else if(pos_name_table == def_pos_table_modu)
             lv_num_item_table  = def_num_colums_table_modu;
    else if(pos_name_table == def_pos_table_pwms)
             lv_num_item_table  = def_num_colums_table_pwms;
    else if(pos_name_table == def_pos_table_dela)
             lv_num_item_table  = def_num_colums_table_dela;

    QString lv_query;
    lv_query.append("SELECT * FROM " + lv_name_table + " ORDER BY id ");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
        while(lv_get_query.next()){
            QStringList lv_row_data;
            for(int f_colum = 1; f_colum < lv_num_item_table; ++f_colum)
                lv_row_data << lv_get_query.value(f_colum).toString();
            vec_data->append(lv_row_data);
        }

}
//*********************************************************************************************************************************************************
// Actualizar los registros de la base de datos
//*********************************************************************************************************************************************************
void jptdatabase::update_register(QString name_table, QString query){
    QString lv_query;
    lv_query.append("UPDATE " + name_table + " SET " + query + ";");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    lv_get_query.exec();

}
//*********************************************************************************************************************************************************
// Actualizar los registros de la base de datos
//*********************************************************************************************************************************************************
void jptdatabase::update_register(QString name_table, QString id, QString query){
    QString lv_query;
    lv_query.append("UPDATE " + name_table + " SET " + query + " WHERE id=" + id + ";");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    lv_get_query.exec();
}

void jptdatabase::update_hub_name(QString hub_name)
{
    QString lv_query;

    lv_query="UPDATE Configurations SET hub_name='"+hub_name+"'  WHERE id=1";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        ////qDebug()<<"Success Query: hub_name";
    }

}


void jptdatabase::update_ip_cloud(QString ip_cloud)
{
    QString lv_query;

    lv_query="UPDATE Configurations SET ip_server='"+ip_cloud+"'  WHERE id=1";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        ////qDebug()<<"Success Query: ip_cloud";
    }

}

void jptdatabase::update_port_cloud(QString port_cloud)
{
    QString lv_query;

    lv_query="UPDATE Configurations SET port_server='"+port_cloud+"'  WHERE id=1";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        ////qDebug()<<"Success Query: port_cloud";
    }

}

void jptdatabase::update_level()
{

    QString lv_query;
    lv_query.append("INSERT INTO nivel_1(time_stamp,nivel,temperature) VALUES('2019-05-27 10:00:01',7,7);");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);
    lv_get_query.exec();

}

QStringList jptdatabase::get_data_config()
{
    QString lv_query;
    lv_query.append("SELECT device FROM config_charts order by id;");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);
    QStringList data_to_send;

    if(lv_get_query.exec())
    {
        while(lv_get_query.next()){
            data_to_send<< lv_get_query.value(0).toString();
            //////qDebug()<<lv_get_query.value(0).toString();

        }
    }
    return data_to_send;


}

void jptdatabase::set_data_config(int id,QString dato)
{
    QString lv_query;

    lv_query="UPDATE config_charts SET device='"+dato+"'  WHERE id="+QString::number(id)+";";
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
    {
        ////qDebug()<<"Success Query: update device"<<id<<"//"<<dato;
    }
}

QString jptdatabase::table_from_device(QString device, int num_sensor,QList<double> list_ing_data)
{
    ////qDebug()<<"TABLE FROM DEVICE"<<device<<num_sensor;
    QString lv_query;

    lv_query="SELECT table_name FROM config_charts WHERE device='"+device+"';";
    ////qDebug()<<lv_query;
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);
    QString data_return="";
    if(lv_get_query.exec())
    {
        if(lv_get_query.next())
        {
            //VERIFICO QUE TIPO DE DATO ES
            ////qDebug()<<"THATS MY ANSWER";
            ////qDebug()<<lv_get_query.size()<<"thats my size ";
            ////qDebug()<<lv_get_query.value(0).toString();

            QStringList data_list=lv_get_query.value(0).toString().split("_");
            ////qDebug()<<data_list[0];
            if(data_list[0]=="nivel")
            {
                //dato de tipo nivel
                QDateTime my_time=QDateTime::currentDateTime();
                QString t_time=my_time.toString("yyyy/MM/dd hh:mm:ss ");

                QString new_query;
                new_query="INSERT INTO "+lv_get_query.value(0).toString()+" (time_stamp,temperature,nivel) VALUES('"+t_time+"'";
                for (int i=0;i<num_sensor;i++) {
                    new_query=new_query+","+QString::number(list_ing_data[i]);
                }
                new_query=new_query+");";
                ////qDebug()<<"thats the query"<<new_query;

                //ahora que tengo la query creada entonces debo de correr la consulta
                QSqlQuery second_query;
                second_query.setForwardOnly(true);
                second_query.prepare(new_query);
                if(second_query.exec())
                {
                    ////qDebug()<<"Consulta realizada con exito";
                }
                else {
                    ////qDebug()<<"consulta sin elemento generado";
                }

            }
            else if(data_list[0]=="ground")
            {
                //dato de tipo UMBRAL
                QDateTime my_time=QDateTime::currentDateTime();
                QString t_time=my_time.toString("yyyy/MM/dd hh:mm:ss ");

                QString new_query;
                new_query="INSERT INTO "+lv_get_query.value(0).toString()+" (time_stamp,acc_X,acc_y,acc_z,humidity,vib_x,vib_y,vib_z) VALUES('"+t_time+"'";
                for (int i=0;i<num_sensor;i++) {
                    new_query=new_query+","+QString::number(list_ing_data[i]);
                }
                new_query=new_query+");";
                ////qDebug()<<"thats the query"<<new_query;

                //ahora que tengo la query creada entonces debo de correr la consulta
                QSqlQuery second_query;
                second_query.setForwardOnly(true);
                second_query.prepare(new_query);
                if(second_query.exec())
                {
                    ////qDebug()<<"Consulta realizada con exito";
                }
                else {
                    ////qDebug()<<"consulta sin elemento generado";
                }


            }
            else if(data_list[0]=="pluvio")
            {
                //dato de tipo nivel
                QDateTime my_time=QDateTime::currentDateTime();
                QString t_time=my_time.toString("yyyy/MM/dd hh:mm:ss ");

                QString new_query;
                new_query="INSERT INTO "+lv_get_query.value(0).toString()+" (time_stamp,precipitation,acum_preci) VALUES('"+t_time+"'";
                for (int i=0;i<num_sensor;i++) {
                    new_query=new_query+","+QString::number(list_ing_data[i]);
                }
                new_query=new_query+");";
                ////qDebug()<<"thats the query"<<new_query;

                //ahora que tengo la query creada entonces debo de correr la consulta
                QSqlQuery second_query;
                second_query.setForwardOnly(true);
                second_query.prepare(new_query);
                if(second_query.exec())
                {
                    ////qDebug()<<"Consulta realizada con exito";
                }
                else {
                    ////qDebug()<<"consulta sin elemento generado";
                }



            }
            else {
                data_return="Data Un-REcognize";
            }




        }



    }
    else {
        data_return="null";
    }
    return data_return;


}
