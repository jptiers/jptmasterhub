#ifndef JPTMDBSLAVE_H
#define JPTMDBSLAVE_H

#include <QObject>
#include <QModbusServer>

#include "_sentence_.h"
#include "class/extern/jptblinkdata.h"

class jptmdbslave : public QObject{
    Q_OBJECT

public:
   explicit jptmdbslave(int id, QStringList params_serial , QString params_tcp, QVector<int> registers, QObject *parent  = nullptr);

   void set_type_modbus(int type);
   void set_port_modbus(QString port);
   void init_modbus(QString n_type);

public slots:
#ifdef _SET_INPUTS_REGISTER
   void set_input_register(const int &value, const int id, const int factor);
#endif
#ifdef _SET_INPUTS_DISCRETE
   void set_input_discrete(const bool &value, const int id);
#endif
#ifdef _SET_HOLDING_REGISTER
    void set_holding_register(const int &value, const int id, const int factor);
#endif
#ifdef _SET_COILS_DISCRETE
    void set_coil_discrete(const int &value, const int id);
#endif

    void init_modbus_connection();
signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif

private:
    QModbusServer *a_modbus_device;

    // CANRIDAD DE REGISTROS PARA INPUTS, COIL, INPUTS, HOLDING
#ifdef _SET_INPUTS_REGISTER
    int a_input_register;
#endif
#ifdef _SET_INPUTS_DISCRETE
    int a_input_discrete;
#endif
#ifdef _SET_HOLDING_REGISTER
    int a_holding_register;
#endif
#ifdef _SET_COILS_DISCRETE
    int a_coil_discrete;
#endif

    // COMUNICACION POR MEDIO DE TCP [0]ip [1]port
    QString a_tcp_port;

    // COMUNICACION POR MEDIO DE SERIAL [0]port [1]parity [2]baud [3]databits [4]stopbits
    QString a_serial_port;
    QString a_serial_parity;
    QString a_serial_baud;
    QString a_serial_dataBits;
    QString a_serial_stopBits;

    int a_id_server;

    void handle_device_error(QModbusDevice::Error new_error);
    void on_connect_type_current_index_changed(int index);
    void setup_device_data();
    void on_connect_modbus(const int type);
};

#endif // JPTMDBSLAVE_H
