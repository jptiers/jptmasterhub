#include "jptmdbslave.h"
#include "jptmdbslave.h"

#include <QModbusRtuSerialSlave>
#include <QModbusTcpServer>
#include <QUrl>

#include <QHostAddress>
#include <QNetworkInterface>
#include <QAbstractSocket>
#include <QHostAddress>
#include <QTimer>

QTimer *a_modbus_reconnect;
int type(0);


enum ModbusConnection{
    Serial,
    Tcp
};
//******************************************************************************************************************************
// Constructor del modbus                                                                                          [jptmdbslave]
//******************************************************************************************************************************
jptmdbslave::jptmdbslave(int id, QStringList params_serial , QString params_tcp,  QVector<int> registers, QObject *parent) : QObject(parent), a_modbus_device(nullptr){

    // CONFIGURACION DE LOS REGISTROS [0]coil [1]inputs discretes [2]inputs [3]holding
#ifdef _SET_INPUTS_REGISTER
    a_input_register    = registers[2];
#endif

#ifdef _SET_INPUTS_DISCRETE
    a_input_discrete    = registers[1];
#endif

#ifdef _SET_HOLDING_REGISTER
    a_holding_register  = registers[3];
#endif

#ifdef _SET_COILS_DISCRETE
    int a_coil_discrete = registers[0];
#endif

    // COMUNICACION POR MEDIO DE TCP port
    a_tcp_port          = params_tcp;

    // COMUNICACION POR MEDIO DE SERIAL [0]port [1]parity [2]baud [3]databits [4]stopbits
    a_serial_port       = params_serial[0];
    a_serial_parity     = params_serial[1];
    a_serial_baud       = params_serial[2];
    a_serial_dataBits   = params_serial[3];
    a_serial_stopBits   = params_serial[4];

    a_id_server         = id;

    a_modbus_reconnect  = new QTimer();
    connect(a_modbus_reconnect, SIGNAL(timeout()), this, SLOT(init_modbus_connection()));
    a_modbus_reconnect->setSingleShot(true);
    a_modbus_reconnect->setInterval(10000);

}

void jptmdbslave::init_modbus_connection(){
    on_connect_modbus(type);
}

//******************************************************************************************************************************
// Inicia la conexion  con modbus                                                                                  [jptmdbslave]
//******************************************************************************************************************************
void jptmdbslave::init_modbus(QString n_type){
    type = (n_type == "TCP") ? 1 : 0;
    on_connect_type_current_index_changed(type);
    on_connect_modbus(type);
}

//******************************************************************************************************************************
// Inicia la conexion  con modbus                                                                                  [jptmdbslave]
//******************************************************************************************************************************
void jptmdbslave::on_connect_modbus(const int type){
    bool lv_intend_to_connect = (a_modbus_device->state() == QModbusDevice::UnconnectedState);
    QString lv_host;

    if(lv_intend_to_connect){
        if(static_cast<ModbusConnection> (type) == Serial){
            a_modbus_device->setConnectionParameter(QModbusDevice::SerialPortNameParameter, a_serial_port);
            a_modbus_device->setConnectionParameter(QModbusDevice::SerialParityParameter, a_serial_parity);
            a_modbus_device->setConnectionParameter(QModbusDevice::SerialBaudRateParameter, a_serial_baud);
            a_modbus_device->setConnectionParameter(QModbusDevice::SerialDataBitsParameter, a_serial_dataBits);
            a_modbus_device->setConnectionParameter(QModbusDevice::SerialStopBitsParameter, a_serial_stopBits);
        }else{
            foreach (const QHostAddress &address, QNetworkInterface::allAddresses())
                if(address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)){
                     lv_host = address.toString();
                     break;
                }
            a_modbus_device->setConnectionParameter(QModbusDevice::NetworkPortParameter, a_tcp_port.toInt());
            a_modbus_device->setConnectionParameter(QModbusDevice::NetworkAddressParameter, lv_host);
        }

        a_modbus_device->setServerAddress(1);

        if (!a_modbus_device->connectDevice() || lv_host.length() < 2){
#ifdef _COMPILE_MESSAGE
            emit viewer_status("[*] mdb");
#endif
            a_modbus_device->disconnect();
            a_modbus_reconnect->start();
        }else
#ifdef _COMPILE_MESSAGE
            emit viewer_status("[o] init modbus {" + lv_host + ":" + a_tcp_port + "} ID:" + QString::number(a_id_server) + " IR:" + QString::number(a_input_register));
#endif
    }else{
        a_modbus_device->disconnectDevice();
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[*] mdb");
#endif
    }
}
//******************************************************************************************************************************
// Funcion que pegara el tipo de comunicacion para modbus, reiniciar la comunicacion                               [jptmdbslave]
//******************************************************************************************************************************
void jptmdbslave::set_type_modbus(int type){
    on_connect_type_current_index_changed(type);
}
//******************************************************************************************************************************
// Funcion que pegara la direccion ip para la comunicacion IP                                                      [jptmdbslave]
//******************************************************************************************************************************
void jptmdbslave::set_port_modbus(QString port){
    a_tcp_port = port;
}
//******************************************************************************************************************************
// Inicializacion de los de los parametros de modbus                                                               [jptmdbslave]
//******************************************************************************************************************************
void jptmdbslave::on_connect_type_current_index_changed(int index){
    if(a_modbus_device){
        a_modbus_device->disconnect();
        delete a_modbus_device;
        a_modbus_device = nullptr;
    }

    ModbusConnection type = static_cast<ModbusConnection> (index);

    if (type == Serial)
        a_modbus_device = new QModbusRtuSerialSlave(this);
    else if (type == Tcp)
        a_modbus_device = new QModbusTcpServer(this);

    if (!a_modbus_device) {
#ifdef _COMPILE_MESSAGE
        if (type == Serial)
            emit viewer_status("[*] mdb slave serial");
        else
            emit viewer_status("[*] mdb server tcp");
#endif
    }else{
        QModbusDataUnitMap reg;
#ifdef _SET_COILS_DISCRETE
        reg.insert(QModbusDataUnit::Coils, { QModbusDataUnit::Coils, 0, quint16(a_coil_discrete)});
#endif
#ifdef _SET_INPUTS_DISCRETE
        reg.insert(QModbusDataUnit::DiscreteInputs, { QModbusDataUnit::DiscreteInputs, 0, quint16(a_input_discrete)});
#endif
#ifdef _SET_INPUTS_REGISTER
        reg.insert(QModbusDataUnit::InputRegisters, { QModbusDataUnit::InputRegisters, 0, quint16(a_input_register)});
#endif
#ifdef _SET_HOLDING_REGISTER
        reg.insert(QModbusDataUnit::HoldingRegisters, { QModbusDataUnit::HoldingRegisters, 0, quint16(a_holding_register)});
#endif

        a_modbus_device->setMap(reg);
        connect(a_modbus_device, &QModbusServer::errorOccurred, this, &jptmdbslave::handle_device_error);

        setup_device_data();
    }
}
//******************************************************************************************************************************
// Recepcion de errores de modbus                                                    SIGNAL[a_modbus_device:errorOcurred - SLOT]
//******************************************************************************************************************************
void jptmdbslave::handle_device_error(QModbusDevice::Error new_error){
    if (new_error == QModbusDevice::NoError || !a_modbus_device)
        return;

#ifdef _COMPILE_MESSAGE
    emit viewer_status("[!] Mdb: " + a_modbus_device->errorString());
#endif

    a_modbus_device->disconnect();
    a_modbus_reconnect->start();
}
//******************************************************************************************************************************
// Inicializar los registros                                                                                       [jptmdbslave]
//******************************************************************************************************************************
void jptmdbslave::setup_device_data(){
    if (!a_modbus_device)
        return;

#ifdef _SET_COILS_DISCRETE
    for(int f_coil = 0; f_coil < a_coil_discrete; ++f_coil)
        a_modbus_device->setData(QModbusDataUnit::Coils, f_coil, 0);
#endif

#ifdef _SET_INPUTS_DISCRETE
    for(int f_input = 0; f_input < a_input_discrete; ++f_input)
        a_modbus_device->setData(QModbusDataUnit::DiscreteInputs, f_input, 0);
#endif

#ifdef _SET_INPUTS_REGISTER
    for(int f_input = 0; f_input < a_input_register; ++f_input){
        a_modbus_device->setData(QModbusDataUnit::InputRegisters, f_input, 0);
    }
#endif

#ifdef _SET_HOLDING_REGISTER
    for(int f_input = 0; f_input < a_input_register; ++f_input)
        a_modbus_device->setData(QModbusDataUnit::HoldingRegisters, f_input, 0);
#endif
}

#ifdef _SET_INPUTS_REGISTER
//******************************************************************************************************************************
// Insertar registros en Input Register                                                                            [jptmdbslave]
//******************************************************************************************************************************
void jptmdbslave::set_input_register(const int &value, const int id, const int factor){
    if (!a_modbus_device)
        return;

    int c_value = factor * value;
    bool lv_status = a_modbus_device->setData(QModbusDataUnit::InputRegisters, id, c_value);

    if(lv_status){
#ifdef _HUB_ON
    #ifdef _BLINK_MODBUS
        jptblinkdata *blink = new jptblinkdata(def_modbus_data);
        blink->start();
    #endif
#endif
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[>] mdb IR");
#endif
    }else{
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[!] mdb {ir}:" + a_modbus_device->errorString());
#endif
    }

}
#endif

#ifdef _SET_INPUTS_DISCRETE
//******************************************************************************************************************************
// Insertar registros en Input Discrete                                                                            [jptmdbslave]
//******************************************************************************************************************************
void jptmdbslave::set_input_discrete(const bool &value, const int id){
    if (!a_modbus_device)
        return;

    bool lv_status = a_modbus_device->setData(QModbusDataUnit::InputRegisters, id, value);

    if(lv_status){
#ifdef _HUB_ON
    #ifdef _BLINK_CLOUD
        jptblinkdata *blink = new jptblinkdata(def_modbus_data);
        blink->start();
    #endif
#endif
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[>] mdb ID");
#endif
    }else{
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[!] mdb {id}:" + a_modbus_device->errorString());
#endif
    }
}
#endif

#ifdef _SET_HOLDING_REGISTER
//******************************************************************************************************************************
// Insertar registros en Holding Register                                                                          [jptmdbslave]
//******************************************************************************************************************************
void jptmdbslave::set_holding_register(const int &value, const int id, const int factor){
    if (!a_modbus_device)
        return;

    int c_value = factor * value;
    bool lv_status = a_modbus_device->setData(QModbusDataUnit::HoldingRegisters, id, c_value);
    if(lv_status){
#ifdef _HUB_ON
    #ifdef _BLINK_MODBUS
        jptblinkdata blink = new jptblinkdata(def_modbus_data);
        blink.start();
    #endif
#endif
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[>] mdb HR");
#endif
    }else{
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[!] mdb {hr}:" + a_modbus_device->errorString());
#endif
    }
}
#endif
#ifdef _SET_COILS_DISCRETE
//******************************************************************************************************************************
// Insertar registros en Coil Discrete                                                                             [jptmdbslave]
//******************************************************************************************************************************
void jptmdbslave::set_coil_discrete(const int &value, const int id){
    if (!a_modbus_device)
        return;

    bool lv_status = a_modbus_device->setData(QModbusDataUnit::Coils, id, value);

    if(lv_status){
#ifdef _HUB_ON
    #ifdef _BLINK_MODBUS
        jptblinkdata blink = new jptblinkdata(def_modbus_data);
        blink.start();
    #endif
#endif
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[>] mdb CD");
#endif
    }else{
#ifdef _COMPILE_MESSAGE
        emit viewer_status("[!] mdb {cd}:" + a_modbus_device->errorString());
#endif
    }
}
#endif


