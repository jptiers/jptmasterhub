#include "jptdevices.h"
#include <QFile>
#include <QtDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDir>
#include <QDateTime>




//jptdevices::jptdevices(QObject *parent) : QObject(parent)
jptdevices::jptdevices()
{
    //La clase solo inicia el path del sistema
    path="/jpt/jptmonitoring/External/devices.json";
    path_devices="/jpt/jptmonitoring/External/Devices/";

}

void jptdevices::run()
{

    //Obteng la cabecera en string
    QString string_id_target;
    if(id_target<1000000)
    {
        string_id_target="00"+QString::number(get_id_targeta());
        ////qDebug()<<"/00"<<string_id_target;
    }
    else if (id_target<10000000) {
       string_id_target="0"+QString::number(get_id_targeta());
       ////qDebug()<<"/0"<<string_id_target;
    }
    else if (id_target<100000000) {
       string_id_target=QString::number(get_id_targeta());
       ////qDebug()<<"/"<<string_id_target;
    }
    else {
        string_id_target=QString::number(get_id_targeta());
        ////qDebug()<<"/"<<string_id_target;
    }

    //Obtengo la hora para la informacion
    QDateTime my_time=QDateTime::currentDateTime();
    //genero la cabecera de mi informacion en pantalla
    QString cabecera="";
    cabecera=cabecera+"[DEVICE: "+get_nombre();
    cabecera=cabecera+" || ID_TARGET: "+string_id_target;
    cabecera=cabecera+" || TIMESTAMP: "+my_time.toString("yyyy/MM/dd hh:mm:ss ")+"]\n";
    cabecera="<font color='green'>"+cabecera+"</font>";

    //obtengo las labels del sistema
    QString label="";
    label="[";
    QStringList labels_list=get_labels_saved();
    for (int i=0;i<labels_list.size();i++)
    {
        label=label+labels_list[i];
        if(i!=labels_list.size()-1)
        {
            label=label+" || ";
        }

    }
    label=label+" ]";
    label="LABELS>>"+label;
    //label="<font color='green'>"+label+"</font>";


    //genero el mensaje de la informacion RAW del sistema.
    QString  raw_message="";
    raw_message="[ ";
    QList<int> temp_data_raw=get_raw_values();
    for(int i=0; i<temp_data_raw.size();i++)
    {
        raw_message=raw_message+QString::number(temp_data_raw[i]);
        if(i!=temp_data_raw.size()-1)
        {
            raw_message=raw_message+" || ";
        }

    }
    raw_message=raw_message+" ]";
    raw_message="RAW DT>>"+raw_message;
    raw_message="<font color='yellow'>"+raw_message+"</font>";

    //obtengo la data de ingenieria
    QString ing_data=calculate_functions();
    QString  ing_message="";
    ing_message="[ ";
    QList<double> temp_data_ing;
    if(ing_data=="C")
    {


        temp_data_ing=get_ing_values();
        for(int i=0; i<temp_data_ing.size();i++)
        {
            ing_message=ing_message+QString::number(temp_data_ing[i]);
            if(i!=temp_data_ing.size()-1)
            {
                ing_message=ing_message+" || ";
            }

        }

        //*************************************************************************************
        //************ En este punto metemos el valor evaluador********************************
        if(is_level==true)
        {
            //si el elemento es un nivel evaluamos si es el primer dato
            if(first_row==true)
            {
                //si es el primer dato entonces iniciamos el valor de la variable.
                first_value=temp_data_ing[1];
                //my_init_time=QDateTime::currentDateTime();
                first_row=false;

            }
            else
            {
                //si no es el primer dato, comparo el dato anterio con el dato actual
                ////qDebug()<<"no es el rpimer dato";
                //si (Dato2-dato anterios 1) > 20) entonces es un pico
                //calculo diferencia
                double diff= temp_data_ing[1]-first_value;
                if(diff>20){
                     //qDebug()<<"pico en el sistema";
                    //si ocurre esto es un pico de señal
                    temp_data_ing[1]=(first_value+5);
                    //ahora seteamos el nuevo valor inicial
                    first_value=(first_value+5);
                }
                else
                {
                    first_value=temp_data_ing[1];
                }

            }
            for(int i=0; i<temp_data_ing.size();i++)
            {
                ////qDebug()<<temp_data_ing[i]<<"Dato de ingenieria";
            }

        }




        //*************************************************************************************
        //*************************************************************************************


    }
    else
    {
        ing_message=ing_data;
    }
    ing_message=ing_message+" ]";
    ing_message="ENG DT>>"+ing_message;
    ing_message="<font color='blue'>"+ing_message+"</font>";

    //En este punto creo la query que persistira a la base de datos.
    //Se Recuerda que la Query es data la cual ira parseada en JSon. dentro de una columna en especifico.
    //El JSON tendra la siguiente arquitectura
    /*
    {
       \"equipo\":
        {
        \"id_equipo\": \"\",
        \"id_board\": \"\",
        \"nombre_equipo\": \"pluviometro\",
        \"cantidad_sensor\": \"2\",
        \"sensors\":
            {
            \"1-precipitacion\": {\"RAW\": \"valor\",\"ENG\": \"Valor\"},
            \"2-precipitacion acumulada\": {\"RAW\": \"valor\",\"ENG\": \"Valor\"}
            }
        }
     }

    */
    QString trame_cloud_database="";
    trame_cloud_database=trame_cloud_database+"{";
    trame_cloud_database=trame_cloud_database+"\"device\":";
    trame_cloud_database=trame_cloud_database+"{";
    trame_cloud_database=trame_cloud_database+"\"device_id\":\""+QString::number(get_id_device())+"\",";
    trame_cloud_database=trame_cloud_database+"\"board_id\": \""+string_id_target+"\",";
    trame_cloud_database=trame_cloud_database+"\"device_name\": \""+get_nombre()+"\",";
    trame_cloud_database=trame_cloud_database+"\"sensor_count\": \""+QString::number(get_num_sensor_saved())+"\",";
    trame_cloud_database=trame_cloud_database+" \"sensors\":";
    trame_cloud_database=trame_cloud_database+"{";


    //en esto punto genero las tramas para los sensors
    for(int i=0;i<get_num_sensor_saved();i++)
    {
        QString ing_data_parse;
        if(!temp_data_ing.isEmpty())
        {
         ing_data_parse.setNum(temp_data_ing[i]);
        }
        else {
         ing_data_parse.setNum(0);
        }
        trame_cloud_database=trame_cloud_database+ "\""+labels_list[i]+"\": {\"RAW\": \""+QString::number(temp_data_raw[i])+"\",\"ENG\": \""+ing_data_parse+"\"}";
        if(i!=get_num_sensor_saved()-1)//pongo la coma al final
        {
            trame_cloud_database=trame_cloud_database+",";
        }

    }

    trame_cloud_database=trame_cloud_database+"}}}"; //con esto cierro todos mis ciclos
    ////qDebug()<<"trame_cloud_database";
    ////qDebug()<<trame_cloud_database;


    //****************************************
    //GEnero una nueva forma de envio de la trama
    QString data_trama_final="";
    //lista de labels: labels_list
    //lista de raw: temp_data_raw
    //lista de ing: temp_data_ing
    data_trama_final="[";
    for(int i=0;i<labels_list.size();i++)
    {
       QString _data_raw=QString::number(temp_data_raw[i]);
       QString _data_eng=QString::number(temp_data_ing[i]);

       data_trama_final=data_trama_final+"<font  color='white'>[ SENSOR: "+labels_list[i]+" </font> <font  color='yellow'>, RAW: "+_data_raw+" </font> <font  color='aqua'>, ENG: "+_data_eng+" ]</font>";
       if(i!=get_num_sensor_saved()-1)//pongo la coma al final
       {
           data_trama_final=data_trama_final+"||";

       }


    }
    data_trama_final=data_trama_final+"]";
    QDateTime my_time_to_send=my_time.addSecs(18000);
    ////qDebug()<<my_time_to_send.toString("yyyy/MM/dd hh:mm:ss ");

    //my_time.setTimeSpec(Qt::UTC);
    my_time_to_send.setTimeSpec(Qt::UTC);

    //uint utc_time=my_time.toTime_t();

    uint utc_time=my_time_to_send.toTime_t();

    ////qDebug()<<utc_time;
    QString time_to_send=QString::number(utc_time);
    //emit signal_to_viewer_reciver(my_time.toString("yyyy/MM/dd hh:mm:ss "),cabecera,data_trama_final,raw_message,ing_message,trame_cloud_database);
    ////qDebug()<<"time to send"<<time_to_send;

    ing_message=my_time.toString("yyyy/MM/dd hh:mm:ss ");
    emit signal_to_viewer_reciver(time_to_send,string_id_target,cabecera,data_trama_final,raw_message,ing_message,trame_cloud_database,string_id_target,get_num_sensor_saved(),temp_data_ing);


}


//******************************************************************************************************************************
// Funcion que calcula el valor de ingenieria, pasado por las ecuaciones                                             [jptabsvar]
//******************************************************************************************************************************
QString jptdevices::calculate_functions()
{
    QString error("C");
    try{
        calculate_function_sensor(get_raw_values());
        try{
            calculate_function_master(get_acumulate_f());
            try{
                calculate_function_correc(get_acumulate_f());
            }catch(...){
                error = "E.F.C";
            }
        }catch(...){
            error = "E.F.M";
        }
    }catch(...){
        error = "E.F.S";
    }

    if(error  == "C")
    { //significa que mi sistema capto todas las formulas bien
      set_sensor_ing(get_acumulate_f());//limpio mi vector de valores

    }
    return  error;

}
//******************************************************************************************************************************
// Funcion  calcula con la ecuacion del sensor tranfer_f                                                         [jptabsvar]
//******************************************************************************************************************************
void jptdevices::calculate_function_sensor(QList<int> value_init)
{
    QString formula="";
    ValueList       lv_vlist;
    FunctionList    lv_flist;
    lv_vlist.AddDefaultValues();
    lv_flist.AddDefaultFunctions();

    QList<double> temp_results;
    QStringList temp_transf=get_transfer_f();
    for(int i=0;i<value_init.size();i++)
    {
       ////qDebug()<<"Hasta este for";
       formula=temp_transf[i];
       //convierto el valor de mi sensor a string y lo remplazo en la formula inicial
       formula.replace("W",QString::number(value_init[i]));
       Expression lv_e;
       lv_e.SetValueList(&lv_vlist);
       lv_e.SetFunctionList(&lv_flist);
       lv_e.Parse(formula.toStdString());
       double temp_double=lv_e.Evaluate();
       temp_results.append(temp_double);///agrego el valor a la funcion

    }
    if(value_init.size()==temp_results.size())
    {
        set_acumulate_f(temp_results);
    }
}
//******************************************************************************************************************************
// Funcion  calcula con la ecuacion del master a_function[1]                                                         [jptabsvar]
//******************************************************************************************************************************
void jptdevices::calculate_function_master(QList<double> value_sensor)
{
    QString formula="";

    ValueList       lv_vlist;
    FunctionList    lv_flist;

    lv_vlist.AddDefaultValues();
    lv_flist.AddDefaultFunctions();

    QList<double> temp_results;
    QStringList temp_master=get_master_f();
    for(int i=0;i<value_sensor.size();i++)
    {
       formula=temp_master[i];
       //convierto el valor de mi sensor a string y lo remplazo en la formula inicial
       formula.replace("W",QString::number(value_sensor[i]));
       Expression lv_e;
       lv_e.SetValueList(&lv_vlist);
       lv_e.SetFunctionList(&lv_flist);

       lv_e.Parse(formula.toStdString());
       double temp_double=lv_e.Evaluate();
       temp_results.append(temp_double);///agrego el valor a la funcion

    }
    if(value_sensor.size()==temp_results.size())
    {
        set_acumulate_f(temp_results);
    }

}
//******************************************************************************************************************************
// Funcion  calcula con la ecuacion del correccion a_function[2]                                                     [jptabsvar]
//******************************************************************************************************************************
void jptdevices::calculate_function_correc(QList<double> value_master)
{
    QString formula="";
    ValueList       lv_vlist;
    FunctionList    lv_flist;

    lv_vlist.AddDefaultValues();
    lv_flist.AddDefaultFunctions();

    QList<double> temp_results;
    QStringList temp_correctf=get_correct_f();
    for(int i=0;i<value_master.size();i++)
    {
       formula=temp_correctf[i];
       //convierto el valor de mi sensor a string y lo remplazo en la formula inicial
       formula.replace("W",QString::number(value_master[i]));
       Expression lv_e;
       lv_e.SetValueList(&lv_vlist);
       lv_e.SetFunctionList(&lv_flist);
       lv_e.Parse(formula.toStdString());
       double temp_double=lv_e.Evaluate();
       temp_results.append(temp_double);///agrego el valor a la funcion



    }
    if(value_master.size()==temp_results.size())
    {
        set_acumulate_f(temp_results);
    }

}
//**************************************************************************************************************
//Funcion que inicia todas las variables mi aplicacion.
//**************************************************************************************************************
void jptdevices::init_device(int id_equipo,int id_targeta,int num_sensor,QList<int> raw_values)
{
    ////qDebug()<<"init devices class devices";
    ////qDebug()<<id_equipo<<"°°"<<id_targeta;
    //seteo mis variables del sistema.
    id_device=id_equipo;
    if(id_equipo==2)
    {
        is_level = true;
        ////qDebug()<<"Thats is a level-temperature sensor";

    }
    id_target=id_targeta;
    num_sensores=num_sensor;
    value_sensors_raw=raw_values;

    //seteo varibles desde el JSON
    nombre=get_nombre_equipo(id_equipo);
    label_sensores=get_labels_sensores(id_equipo);
    set_transfer_f(get_transfer_f(id_equipo,"T"));//seteamos las funciones de transferencia
    set_master_f(get_transfer_f(id_equipo,"M"));//seteamos funciones maestras
    set_correct_f(get_transfer_f(id_equipo,"C"));//seteamos funciones de correccion por defecto.

    /* Aparte de cargar la funciones de transferencia
     * el sistema debera verificar si existe un archivo de funciones de transferencia en la carpeta external
     * Si el archivo existe no hara nada, en caso contrario lo creara y le pondra por defecto las caracteristicas
     * del JSON de equipos.
     * de configuracion dentro de la carpeta external para la calibracion de las funciones de transferencia en caso contrario
     */

     //1* Genero el nombre del TXT que se debe buscar en sistema.
     QString name_txt="ID-"+QString::number(id_targeta)+".txt";
     txt_funcion_create(name_txt);

}
//**************************************************************************************************************
//GETTERS & SETTERS DEL SISTEMA, PARA LAS VARIABLES PRIVADAS DE ALAMACENAMIENTO.
//**************************************************************************************************************
//Getter Id_targeta
int jptdevices::get_id_targeta()
{
    return id_target;

}
//Getter Id_device
int jptdevices::get_id_device()
{
    return id_device;

}

//Getter num sensors
int jptdevices::get_num_sensor_saved()
{
    return num_sensores;

}

//Setter sensor_raw
void jptdevices::set_sensor_raw(QList<int> values)
{
    //setea los valores raw
    value_sensors_raw.clear();
    value_sensors_raw=values;
    //Una vez obtenida la informacion Raw debemos de porcesar la data para obtener los valores de ing.

}
//Getter list of raw values
QList<int> jptdevices::get_raw_values()
{
    return value_sensors_raw;

}

//Setter sensor_ing
void jptdevices::set_sensor_ing(QList<double> values)
{
    //setea los valores raw
    value_sensors_ing.clear();
    value_sensors_ing=values;

}
//Getter list of ing values
QList<double> jptdevices::get_ing_values()
{
    return value_sensors_ing;

}

//Setter acumulate values
void jptdevices::set_acumulate_f(QList<double> values)
{
    //setea los valores raw
    acumlator_functions.clear();
    acumlator_functions=values;

}
//Getter acumulator functions values
QList<double> jptdevices::get_acumulate_f()
{
    return acumlator_functions;

}

QStringList jptdevices::get_transfer_f()
{
    return transfer_f;
}
void jptdevices::set_transfer_f(QStringList funciones)
{
    transfer_f.clear();
    transfer_f=funciones;
}

QStringList jptdevices::get_master_f()
{
    return master_f;
}
void jptdevices::set_master_f(QStringList funciones)
{
    master_f.clear();
    master_f=funciones;
}


QStringList jptdevices::get_correct_f()
{
    return correct_f;
}
void jptdevices::set_correct_f(QStringList funciones)
{
    correct_f.clear();
    correct_f=funciones;
}

QStringList jptdevices::get_labels_saved()
{
    return label_sensores;
}

//Getter Nombre
QString jptdevices::get_nombre()
{
    return nombre;
}

//**************************************************************************************************************
//funcion que retorna la cantidad de sensors.
//**************************************************************************************************************
int jptdevices::get_num_sensores(int equipo_id)
{
    int r=0;
    ////qDebug()<<"DAto de entrada"<<equipo_id;

    QFile file(path);
    if(!file.exists())
    {
        //qDebug()<<"No existe la base de datos de los equipos";

    }
    else{

        //realiza esta funcion cuando ha encontrado el archivo de informacion de los clientes
        QString line;//line from file to save the data.
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&file);

            QString data=stream.readAll();

            //ya se obtuvo la informacion del Json es tiempo de parsear la misma.
            data=data.replace("\n","");

           ////qDebug()<<data;

            QJsonDocument lv_message_json = QJsonDocument::fromJson(data.toUtf8());
            QJsonObject   lv_trama_json   = lv_message_json.object();
            ////qDebug()<<lv_trama_json.length()<<"longitud de mi JSON";
            QStringList parce_list_label=lv_trama_json.keys();

            for(int i=0;i<lv_trama_json.length();i++)
            {

                ////qDebug()<<"id_equipo :"<<parce_list_label[i].toUtf8();
                int id_device=parce_list_label[i].toInt();
                if(equipo_id==id_device)
                {//si los id concuerdan retorna el valor de la cantida de sensors.
                  QString name_parce=parce_list_label[i].toUtf8();
                  QJsonObject data_client=lv_trama_json.value(name_parce).toObject();

                   r=data_client.value("sensor_count").toString().toInt();
                   ////qDebug()<<r<<"Valor de R";
                  //r=_temp.toInt();

                }


            }

         }
         file.close();



    }



    return r;


}
//**************************************************************************************************************
//funcion que retorna el nombre del equipo
//**************************************************************************************************************
QString jptdevices::get_nombre_equipo(int equipo_id)
{
    QString respuesta;
    QFile file(path);
    if(!file.exists())
    {
        ////qDebug()<<"No existe la base de datos de los equipos";

    }
    else{

        //realiza esta funcion cuando ha encontrado el archivo de informacion de los clientes
        QString line;//line from file to save the data.
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&file);

            QString data=stream.readAll();

            //ya se obtuvo la informacion del Json es tiempo de parsear la misma.
            data=data.replace("\n","");
            ////qDebug()<<data;

            QJsonDocument lv_message_json = QJsonDocument::fromJson(data.toUtf8());
            QJsonObject   lv_trama_json   = lv_message_json.object();
            ////qDebug()<<lv_trama_json.length()<<"longitud de mi JSON";
            QStringList parce_list_label=lv_trama_json.keys();

            for(int i=0;i<lv_trama_json.length();i++)
            {

                ////qDebug()<<"id_equipo :"<<parce_list_label[i].toUtf8();
                int id_device=parce_list_label[i].toInt();
                if(equipo_id==id_device)
                {//si los id concuerdan retorna el valor de la cantida de sensors.
                  QString name_parce=parce_list_label[i].toUtf8();
                  QJsonObject data_client=lv_trama_json.value(name_parce).toObject();
                  respuesta=data_client.value("device_name").toString();

                }

            }

         }
         file.close();

    }

    return respuesta;



}
//**************************************************************************************************************
//funcion que retorna una lista con los labels de los sensors del sistema
//**************************************************************************************************************
QStringList jptdevices::get_labels_sensores(int equipo_id)
{
    QStringList respuesta;
    QFile file(path);
    if(!file.exists())
    {
        //qDebug()<<"No existe la base de datos de los equipos";

    }
    else{

        //realiza esta funcion cuando ha encontrado el archivo de informacion de los clientes
        QString line;//line from file to save the data.
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&file);

            QString data=stream.readAll();

            //ya se obtuvo la informacion del Json es tiempo de parsear la misma.
            data=data.replace("\n","");
            ////qDebug()<<data;

            QJsonDocument lv_message_json = QJsonDocument::fromJson(data.toUtf8());
            QJsonObject   lv_trama_json   = lv_message_json.object();
            ////qDebug()<<lv_trama_json.length()<<"longitud de mi JSON";
            QStringList parce_list_label=lv_trama_json.keys();

            for(int i=0;i<lv_trama_json.length();i++)
            {

                ////qDebug()<<"id_equipo :"<<parce_list_label[i].toUtf8();
                int id_device=parce_list_label[i].toInt();
                if(equipo_id==id_device)
                {//si los id concuerdan retorna el valor de la cantida de sensors.
                  QString name_parce=parce_list_label[i].toUtf8();
                  QJsonObject data_client=lv_trama_json.value(name_parce).toObject();

                  QJsonObject sensors_object =data_client.value("sensors").toObject();

                  respuesta=sensors_object.keys();

                }


            }

         }
         file.close();

    }

    return respuesta;


}
//**************************************************************************************************************
//funcion que retorna una lista con las funciones de transferencia
//**************************************************************************************************************
QStringList jptdevices::get_transfer_f(int equipo_id,QString funtion)
{
    QStringList respuesta;

    QFile file(path);
    if(!file.exists())
    {
        //qDebug()<<"No existe la base de datos de los equipos";

    }
    else{

        //realiza esta funcion cuando ha encontrado el archivo de informacion de los clientes
        QString line;//line from file to save the data.
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&file);
            QString data=stream.readAll();
            //ya se obtuvo la informacion del Json es tiempo de parsear la misma.
            data=data.replace("\n","");
            ////qDebug()<<data;
            QJsonDocument lv_message_json = QJsonDocument::fromJson(data.toUtf8());
            QJsonObject   lv_trama_json   = lv_message_json.object();
            ////qDebug()<<lv_trama_json.length()<<"longitud de mi JSON";
            QStringList parce_list_label=lv_trama_json.keys();

            for(int i=0;i<lv_trama_json.length();i++)
            {

                ////qDebug()<<"id_equipo :"<<parce_list_label[i].toUtf8();
                int id_device=parce_list_label[i].toInt();
                if(equipo_id==id_device)
                {//si los id concuerdan retorna el valor de la cantida de sensors.
                  QString name_parce=parce_list_label[i].toUtf8();
                  QJsonObject data_client=lv_trama_json.value(name_parce).toObject();

                  QJsonObject sensors_object =data_client.value("sensors").toObject();
                  //obtengo en este punto mis label de sensors
                  QStringList label_sensor=sensors_object.keys();

                  ////qDebug()<<"Label sensor tam"<<label_sensor.size();
                  for (int k=0;k<label_sensor.size();k++) {
                      //se obtiene el label del sensor
                      QString label_parce=label_sensor[k].toUtf8();
                      ////qDebug()<<sensors_object.value(label_parce).toString();
                      //obtengo el valor de los datos
                      QJsonObject transfer_functions=sensors_object.value(label_parce).toObject();
                      //qDebug()<<transfer_functions.value("T").toString();
                      //agrego mi elemento a la respuesta del sistema
                      respuesta.append(transfer_functions.value(funtion).toString());
                  }
                }


            }

         }
         file.close();

    }

    return respuesta;



}
//**************************************************************************************************************
//funcion que genera un txt con las funciones de transferencia del equipo si no existe el txt.
//**************************************************************************************************************
void jptdevices::txt_funcion_create(QString name_txt)
{

    //Se carga el path de conexion o almacenmiento del archivo .txt con la ip.
    QString path_source = path_devices+name_txt;
    QStringList  my_label_list=get_labels_saved();

    QFile file(path_source);
    if(!file.exists()){
        //qDebug() << "NO existe el archivo de funciones de transferencia de equipos: "<<name_txt;
        //qDebug()<<"Se procede a crear el archivo";
        //como no existe el archivo procedemos a generar la cadena por primera vez.
        QString data_txt;
        for (int i=0;i<correct_f.size();i++)
        {
            data_txt=data_txt+my_label_list[i]+">>"+correct_f[i]+"\n";
        }


        if(!QDir("External/Devices").exists() )
        {
            //si no existe el directorio se crea.
            //qDebug()<<"No existe el directorio de almacenamiento Devices, se procede a crearse";
            QDir().mkdir("External/Devices");
            //qDebug()<<"Directorio Devices Creado";
            QFile archivo(path_source);
            //qDebug()<<"Archivo de Device Creado";
            if(archivo.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                    QTextStream datosArchivo(&archivo);
                    datosArchivo << data_txt<<endl;
                    ////qDebug()<<json_clients;

           }
            archivo.close();
            txt_funcion_create(name_txt);

        }
        else {
            QFile archivo(path_source);
            //qDebug()<<"Archivo Device Creado";
                if(archivo.open(QIODevice::WriteOnly | QIODevice::Text)){
                    QTextStream datosArchivo(&archivo);
                    datosArchivo << data_txt<<endl;
                    //qDebug()<<data_txt;

                }
            archivo.close();

            txt_funcion_create(name_txt);

        }

    }
    else {
        //como el elmento ya esta creado entonces emito una señal de creacion de datos

        update_correct_f();

        QString data_combo_box;
        //la señal es el numero de id , seguido del nombre
        data_combo_box=""+QString::number(get_id_targeta())+">>"+get_nombre();
        emit combo_box_element(data_combo_box);
        //cuando la señal se emite se genera el elemento dentro del combo box.
    }

}


void  jptdevices::update_correct_f()
{
    //funcion que actualiaz las funciones de correccion
    //optengo el nombre del archivo
    QString data_txt="ID-"+QString::number(get_id_targeta());//genero el elemento a consultar
    //obtengo el numero de sensores
    int my_num_sensor=get_num_sensor_saved();

    ////qDebug()<<"Dentro de devices"<<data_txt;

    QStringList temp_funcions;

    //Se carga el path de conexion o almacenmiento del archivo .txt
    QString path_source ="/jpt/jptmonitoring/External/Devices/"+data_txt+".txt";

    QFile file(path_source);//leo el archivo si existe
    if(!file.exists())
    {
        //qDebug() << "NO existe el archivo de funciones de transferencia de equipos: ID-"<<path_source;

    }
    else
    {//leo mi archivo
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream in(&file);
               while (!in.atEnd())
               {
                   temp_funcions.append(in.readLine());

               }

        }
        file.close();
    }// fin del ciclo else
    QStringList line;
    if(!temp_funcions.isEmpty()){
        //sio es diferente de estar vacia entonces la recorro
        for(int i=0;i<my_num_sensor;i++)
        {   line.clear();
            line=temp_funcions[i].split(">>");
           // //qDebug()<<temp_funcions[i];
            temp_funcions[i]=line[1];
            ////qDebug()<<temp_funcions[i];
        }
        //ahora cargo mi lista dentro de la lsita de correcciones
        set_correct_f(temp_funcions);
    }


}






