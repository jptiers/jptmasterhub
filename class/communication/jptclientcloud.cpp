#include "jptclientcloud.h"
#include <QDebug>

#define def_delay_send_message    1000
#define def_delay_send_node       1600000
#define def_lim_voltaje_requerido 1200
#define def_wait_conn_Host        20000

bool gv_status_client = false;
QTimer *gv_timer_send_cloud;
QTimer *gv_timer_level_energy;

//******************************************************************************************************************************
// Funcion principal                                                                                            [jptclientcloud]
//******************************************************************************************************************************
jptclientcloud::jptclientcloud(const QString ip_cloud, const int port_cloud, QObject *parent) : QObject(parent), a_ip_cloud(ip_cloud), a_port_cloud(port_cloud){
    //----------------------------------------------------------------------------------------------------------------
    // conexion con el servidor nube                                                                      [QTcpSocket]
    //----------------------------------------------------------------------------------------------------------------
    a_socket_client = new QTcpSocket(this);

    connect(a_socket_client, SIGNAL(readyRead()), this, SLOT(message_from_server()));
#ifdef _COMPILE_MESSAGE
    connect(a_socket_client, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(display_error(QAbstractSocket::SocketError)));
#endif
    a_socket_client->connectToHost(QHostAddress(ip_cloud), port_cloud);
    ////qDebug()<<a_socket_client->state()<<ip_cloud<<port_cloud;
    //forzamos la conexion con el servidor
    //////qDebug()<<"starting the socket TCP";
    //restart_socket();


    //----------------------------------------------------------------------------------------------------------------
    // Thread que vigila la conexion a internet                                                          [jptethernet]
    //----------------------------------------------------------------------------------------------------------------
    a_watch_status_ethernet = new jptethernet();
#ifdef _COMPILE_MESSAGE
    connect(a_watch_status_ethernet, SIGNAL(viewer_status(QString)), this, SLOT(viewer_status_(QString)));
#endif
    connect(a_watch_status_ethernet, SIGNAL(status_ethernet(bool)), this, SLOT(status_ethernet_(bool)));
    connect(a_watch_status_ethernet, SIGNAL(restart_beagle_signal()), this, SLOT(restart_beagle_signal()));
    a_watch_status_ethernet->start();

    //----------------------------------------------------------------------------------------------------------------
    // Timer que envia los datos segun un tiempo                                                              [QTimer]
    //----------------------------------------------------------------------------------------------------------------
    gv_timer_send_cloud = new QTimer();
    gv_timer_send_cloud->setInterval(def_delay_send_message);
    gv_timer_send_cloud->setSingleShot(true);
    connect(gv_timer_send_cloud, SIGNAL(timeout()), this, SLOT(message_to_server()));

    //----------------------------------------------------------------------------------------------------------------
    // Timer que verifica cada 30min el estado de carga de la beagle                                          [QTimer]
    //----------------------------------------------------------------------------------------------------------------
    gv_timer_level_energy = new QTimer();
    gv_timer_level_energy->setInterval(def_delay_send_node);
    connect(gv_timer_level_energy, SIGNAL(timeout()), this, SLOT(read_volt_hub()));
    gv_timer_level_energy->start();
}
//******************************************************************************************************************************
// Slot que conectarar la funcion principal con la senal proveniente de verificacion de internet SIGNAL[jptethernet:restart.. SLOT]
//******************************************************************************************************************************
void jptclientcloud::restart_beagle_signal(){
    emit restart_beagle_signal_ethernet();
#ifdef _COMPILE_MESSAGE
    emit viewer_status("[!] reset BBB");
#endif
}
//******************************************************************************************************************************
// Funcion que emite la lectura del conversor analogo digital para enviar a la nube                  SIGNAL[QTimer:timeout SLOT]
//******************************************************************************************************************************
void jptclientcloud::read_volt_hub(){
    jptenergylevel * read_adc = new jptenergylevel();
    connect(read_adc, SIGNAL(value_volts_and_current(QString,QString,QString)), this, SLOT(value_volts_and_current_slot(QString,QString,QString)));
    read_adc->start();
}
//******************************************************************************************************************************
// Funcion que recibe y emite la senal proveniente del thread de lectura del A/D para el voltage     SIGNAL[QTimer:timeout SLOT]
//******************************************************************************************************************************
void jptclientcloud::value_volts_and_current_slot(QString value_1, QString value_2, QString name_node){
    emit value_volts_and_current_signal(value_1, value_2, name_node);
#ifdef _COMPILE_MESSAGE
    emit viewer_status("[<] {Volt:" + value_1 + ", Curr:" + value_2 + "}");
#endif
    bool nivel = (value_1.toInt() < def_lim_voltaje_requerido) ? false : true;
    emit status_voltage(nivel, 1);
}
//******************************************************************************************************************************
// Funcion que recibe la respuesta proveniente del servidor Listener (python)                  SIGNAL[QTcpSocket:readyRead SLOT]
//******************************************************************************************************************************
void jptclientcloud::message_from_server(){
    if(a_buffer_data.size() > 0){
        QString lv_message_server_cloud = a_socket_client->readAll();

        if(lv_message_server_cloud == "non"){
#ifdef _COMPILE_MESSAGE
            emit viewer_status("[<] Cloud SK:[" + QString::number(a_buffer_data.size()) + "] R: " + lv_message_server_cloud);
#endif
        }else{
            a_buffer_data.removeFirst();


#ifdef _COMPILE_MESSAGE
            emit viewer_status("[<] Cloud SK:[" + QString::number(a_buffer_data.size()) + "] R: " + lv_message_server_cloud);
#endif
        }
        if(a_buffer_data.size() > 0)
            gv_timer_send_cloud->start();
    }
}
//******************************************************************************************************************************
// Reiniciar el socket que comunica con el servidor                                                             [jptclientcloud]
//******************************************************************************************************************************
void jptclientcloud::restart_socket(){
    if(!a_socket_client->state() == QAbstractSocket::ConnectingState){
        a_socket_client->close();
        ////qDebug()<<"tratamo de conectar al servidor";
        a_socket_client->connectToHost(QHostAddress(a_ip_cloud), a_port_cloud);
        a_socket_client->waitForConnected(10000);
    }
    check_connect_cloud();

}
//******************************************************************************************************************************
// Verifica que el socket este bien configurado                                                                 [jptclientcloud]
//******************************************************************************************************************************
void jptclientcloud::check_connect_cloud(){
    a_status_cloud = (a_socket_client->state() == QAbstractSocket::ConnectedState) ? true : false;
#ifdef _COMPILE_MESSAGE
    QString lv_output = (a_status_cloud) ? "[o] Cloud" : ("[*] Cloud " + a_ip_cloud + ":" + QString::number(a_port_cloud));

    emit viewer_status(lv_output);
#endif
}
//******************************************************************************************************************************
// Envia un mensaje al servidor de la nube                                                                      [jptclientcloud]
//******************************************************************************************************************************
void jptclientcloud::send_message_cloud(QString message){
    a_socket_client->write(message.toLatin1().data());
#ifdef _COMPILE_MESSAGE
    emit viewer_status("[>] Cloud");
#endif
}
//******************************************************************************************************************************
// apilamiento de la informacion que va para la nube                                                            [jptclientcloud]
//******************************************************************************************************************************
void jptclientcloud::stack_data(QString message){
    if(message != "null"){
        a_buffer_data.append(message);
        gv_timer_send_cloud->start();
    }
}
//******************************************************************************************************************************
// Envia una trama a el servidor                                                                   SIGNAL[QTimer:timeour - SLOT]
//******************************************************************************************************************************
void jptclientcloud::message_to_server(){
    check_connect_cloud();

    if(!a_status_cloud)
        restart_socket();

    if(a_status_ethernet && a_status_cloud)
        send_message_cloud(a_buffer_data.constFirst());
}
#ifdef _COMPILE_MESSAGE
//******************************************************************************************************************************
// Visualizar el estado de la nube y ethernet          SIGNAL[jptethernet:viewer_status - SLOT] -> [jptMonitoring:viewer_status]
//******************************************************************************************************************************
void jptclientcloud::viewer_status_(QString status){
    emit viewer_status(status);
}
#endif
//******************************************************************************************************************************
// Verifica el estado del internet   SIGNAL[jptethernet:status_ethertet - SLOT] -> [jptmonitoring:status_ethernet, status_cloud]
//******************************************************************************************************************************
void jptclientcloud::status_ethernet_(bool status){
    a_status_ethernet = status;
    if(!status)
        if((!a_status_cloud) || (a_buffer_data.size() != 0))
            restart_socket();

    emit status_ethernet(status, 2);
}
//******************************************************************************************************************************
// Destructor                                                                                                   [jptclientcloud]
//******************************************************************************************************************************
jptclientcloud::~jptclientcloud(){
    a_socket_client->close();
}
//******************************************************************************************************************************
// Errores que provienen de abrir el servidor [Socket]                                                              [QTcpSocket]
//******************************************************************************************************************************
#ifdef _COMPILE_MESSAGE
void jptclientcloud::display_error(QAbstractSocket::SocketError socketError){
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        emit viewer_status("[e]-[cld] Close");
        break;
    case QAbstractSocket::HostNotFoundError:
        emit viewer_status("[e]-[cld] Not Found");
        break;
    case QAbstractSocket::ConnectionRefusedError:
        emit viewer_status("[e]-[cld] Refused");
        break;
    default:
        emit viewer_status("[e]-[cld] " + a_socket_client->errorString());
    }
    //independiente del error que se genere se debe realizar un restar network.
    restart_socket();
}
#endif


//*********************************************************************************************************************************
//*********************************************************************************************************************************
//*********************************************************************************************************************************

void jptclientcloud::set_ip_cloud(QString ip_cloud){
    a_ip_cloud=ip_cloud;
}

void jptclientcloud::set_port_cloud(int port){
    a_port_cloud=port;
}

void jptclientcloud::system_restart(){

    a_socket_client->close();
    ////qDebug()<<a_ip_cloud<<":"<<a_port_cloud;

    a_socket_client->connectToHost(QHostAddress(a_ip_cloud), a_port_cloud);
    a_socket_client->waitForConnected(10000);

    ////qDebug()<<a_socket_client->state();
    check_connect_cloud();
}


