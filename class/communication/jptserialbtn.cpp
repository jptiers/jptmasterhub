#include "jptserialbtn.h"
#include <QtDebug>
#include <QTextCodec>
#include <stdlib.h>


#define def_num_count_serial 20

QString     gv_serial_trame_btn("");
bool        gv_serial_connecting_btn(false);
QString a_serial_portnumber="";
//******************************************************************************************************************************
// Inicio del objeto de comunicacion serial
//******************************************************************************************************************************
jptserialbtn::jptserialbtn(QString serial_factory, int baud_rate, QObject *parent) : QObject(parent){
    qDebug()<<"Serial Factory>>"<<serial_factory;
    init_var(serial_factory);
    a_baud_rate         = baud_rate;
    a_serial_port       = new QSerialPort(this);
    a_serial_reconnect  = new QTimer();
    connect(a_serial_reconnect, SIGNAL(timeout()), this, SLOT(init_serial_connection()));
}
//******************************************************************************************************************************
// Inicio la funcion de configuracion del puerto
//******************************************************************************************************************************
void jptserialbtn::init_serial_connection(){
    try_connect_serial();
}
//******************************************************************************************************************************
//  Inicia las variables que describen el dispositivo al cual se conectara el programa para recepcion de los datos
//******************************************************************************************************************************
void jptserialbtn::init_var(QString serial_factory){
    //funcion que inicia el serial factory con l data del serial
    QStringList lv_temp_serial = serial_factory.split("-");

    if(lv_temp_serial.length() > 1){
        a_serial_factory    = lv_temp_serial[0];
        a_serial_number     = lv_temp_serial[1];
        a_serial_portnumber = lv_temp_serial[2];
    } else{
        a_serial_factory    = serial_factory;
        a_serial_number     = "";
    }
}
//******************************************************************************************************************************
// ~
//******************************************************************************************************************************
jptserialbtn::~jptserialbtn(){
    a_serial_reconnect->destroyed();
    a_serial_port->close();
}
//******************************************************************************************************************************
// Inicia el timer que permitira verificar que la conexion este abierta
//******************************************************************************************************************************
void jptserialbtn::init_timer_reconnect(){
    a_serial_reconnect->setInterval(def_timer_delay_reconnect_serial);
    a_serial_reconnect->setSingleShot(true);
    a_serial_reconnect->start();
}
//******************************************************************************************************************************
// SLOT que verifica la desconexion del dispositivo
//******************************************************************************************************************************
void jptserialbtn::serial_unplugged(){
    if(!gv_serial_connecting_btn){
        a_serial_port->close();
        gv_serial_trame_btn.clear();

#ifdef _COMPILE_MESSAGE
        emit viewer_status("[?] Serial FTDI");
#endif
        init_timer_reconnect();
        gv_serial_connecting_btn = true;
    }
}
//******************************************************************************************************************************
// SLOT que permite obtener los valores de la lectura serial
//******************************************************************************************************************************
void jptserialbtn::read_serial_port()
{

    //qDebug()<<a_serial_port->bytesAvailable()<<">>bytes aviable";//SOLO ME DICE LA CANTIDAD DE ELEMENTOS PENDIENTES DE LECTURA.

    gv_serial_trame_btn.clear();
    //leo toda la informacion del paquete de llegada.
    QByteArray my_byte=a_serial_port->readAll();

    while(a_serial_port->canReadLine())
    {
        gv_serial_trame_btn     += QString::fromLatin1(a_serial_port->readLine());


    }
    //ahora pasa mis datos a string
    QString dd=QString(my_byte.toHex());
    qDebug()<<dd<<"dd";
    if(dd=="3832")//valid si se presiono el boton
    {
        emit viewer_status(">>>Button Siren Pressed<<<");
        emit signalButttonFtdi();
    }


}
//******************************************************************************************************************************
// funcion encargada del manejo de los datos de los sensores.
//******************************************************************************************************************************
void jptserialbtn::parse_sensores(int id_equipo,int id_target,int num_sensores,QStringList _sensores,QString _random, QString check_sum)
{
   //lo primero es obtener el numero decimal equivalente de mis datos
   QList<int> sensor_list;
   for(int i=0;i<_sensores.size();i++)
   {   //pasamos cada valor a la lista de enteros desde la lista de strings convirtiendo enel proceso string to int.
       sensor_list.append(get_int_from_string(_sensores[i]));
   }
   //obtenemos el equivalente de entero del random y del checksum
   int my_random=get_int_from_string(_random);

   //int my_check_sum=get_int_from_string(check_sum);//el checksum ya se valido no es necesario en esta nueva funcion

   QList<int> sensor_dec;//esta es mi list de sensores decodificados

   for (int i=0;i<sensor_list.size();i++)
   {
    sensor_dec.append(sensor_list[i] ^ my_random );
    ////qDebug()<<"Mi valor de random es:  "<<_random<<" En decimal: "<<my_random;
    ////qDebug()<<"Mi valor de sensor es:  "<<_sensores[i]<<" En decimal es: "<<sensor_list[i];
    ////qDebug()<<"Mi valor recuperado es: "<<sensor_dec[i];
   }

   //Antes de proceder a verificar la informacion se verifica el check_sum

   ////qDebug()<<"Emito los datos al monitoring";
   //una vez parseada la data y verificada la misma enviamos al monitoring para su procesamiento
   emit viewer_status("[<] Sensor Entry : "+QString::number(id_target));
   emit send_trame_to_monitoring(id_equipo,id_target,num_sensores,sensor_dec);



}
//******************************************************************************************************************************
// funcion encargada de convertir el valor hexadecimal almacenado en string a entero decimal
//******************************************************************************************************************************
int  jptserialbtn::get_int_from_string(QString _data)
{
    //primero debemos convertir la data a hexadecimal
    bool bstatus=false;
    uint nhex=_data.toUInt(&bstatus,16);
    ////qDebug()<<nhex;
    int my_int=0;
    if(bstatus)
    {
         my_int=int(nhex);
    }
    return my_int;
}


void jptserialbtn::get_trama_from_stack()
{
    //este ciclo me mostrara la pila que se esta creando

    qDebug()<<"MI PILA****************** ";
    QString shw="";
    for (int i=0;i<my_stack.count();i++)
    {
        shw=shw+my_stack.at(i);


    }
    qDebug()<<shw;


    ////qDebug()<<"Entro al ciclo de obtencion de la pila";
    if(my_stack.count()>=15)//significa que mi trama tiene el tamaño minimo posible
    {
        for (int j=0;j<my_stack.count();j++)
        {
            if(my_stack.at(j)=="8d")
            {
              cabecera_num=j;//guardo la posicion donde esta la cabecera de la trama
              cabecera_bool=true;
              ////qDebug()<<"cabecera:"<<cabecera_num;
            }
            else if(j!=my_stack.count()-1)
            {
                //para validar deben de quedar por lo menos dos datos
                //por eso no puedo estar parado en la posicion final
                if(my_stack.at(j)=="0d" && my_stack.at(j+1)=="0a")//valido mi cola
                {
                    cola_num=j;
                    ////qDebug()<<"cola:"<<cola_num;
                    if(cabecera_bool==true)//si existe ya una cabecera en el sistema
                    {
                        if(cabecera_num>cola_num)
                        {//debemos de borrar todo hasta ese punto y romper el ciclo
                            for (int i=0;i<cola_num+1;i++) {
                                my_stack.dequeue();
                            }
                            //ahora clereo la variable booleana del sistema
                            cabecera_bool=false;
                            break;
                        }


                        ////qDebug()<<"Existe cabecera y cola en este punto.";
                        //En ese caso debo armar mi trama, desde la posicion cabecera_num hasta cola_num
                        QString temp_trame="";
                        for (int i=cabecera_num;i<cola_num+2;i++)//recordemos que la posicion de la cola es 0d, y cola mas 1 es 0a.
                        {
                          temp_trame=temp_trame+my_stack.at(i);
                        }
                        ////qDebug()<<temp_trame<<"Esa es ala trama del sistema.";
                        //en este punto realizo el proceso de verificacion de mi trama.
                        verificar_trama(temp_trame);

                        //ya armada mi trama debo de borrar todos los datos hasta mi cola
                        for (int i=0;i<cola_num+1;i++) {
                            my_stack.dequeue();
                        }
                        //ahora clereo la variable booleana del sistema
                        cabecera_bool=false;
                        //puede que exista otra trama
                        //invocamos de nuevo la funcion
                        get_trama_from_stack();

                    }
                    else
                    {
                        ////qDebug()<<"no verifica nada del sistema";
                    }

                }


            }


        }
    }
    else {
        ////qDebug()<<"Tamaño minimo no alcanzado";
    }
}

void jptserialbtn::verificar_trama(QString _trame)
{
    ////qDebug()<<_trame;
    //1. obtener el tipo de equipo que tenemos de llegada
    QString tipo_equipo=_trame.mid(4,2);
    int _equipo=get_int_from_string(tipo_equipo);
    QString tipo_targeta=_trame.mid(6,2);
    QString tipo_targeta_2=_trame.mid(8,4);


    int _target=get_int_from_string(tipo_targeta);
    int _target_2=get_int_from_string(tipo_targeta_2);

    _target=(_target*100000)+_target_2;

    int num_sensores=device->get_num_sensores(_equipo);//este es un ejemplo de como va la funcion
    ////qDebug()<<"Valido si funciona el retorno de datos"<<num_sensores;

    int _sensores=get_int_from_string(_trame.mid(12,4));
    ////qDebug()<<"sensores tomados de la trama"<<_sensores;


    //valida los sensores con la cantidad de la trama, en caso contrario descarta la trama
    //Valido tambien cantdad de datos de la trama
    int datos=20+6+(4*_sensores);
    if(num_sensores==_sensores && datos==_trame.size())
    {
        //Obtengo el ramdom del sistema, que me servira para restructurar el valor de cada sensor
        QString _random=_trame.mid(16,4);
        ////qDebug()<<"RAndom:"<<_random;
        //si el sistema es igual entonces comienza a realizar el parseo de cada sensor
        QStringList sensores;
        for(int i=0;i<num_sensores;i++)//parseamos cada sensor
        {
           //la posicion de los sensores sera 18+4*i
           //dado que cada sensores son dos bytes, es decir 4 numeros Hezadecimales
           int parse=20+(4*i);
           sensores.append(_trame.mid(parse,4));
        }

        //por ultimo obtengo el Check-Sum que se me envia en la trama
        QString check_sum=_trame.mid(_trame.size()-6,2);
        ////qDebug()<<"Check-sum>>"<<check_sum;

        //calculo mi check_sum
        QByteArray carga_util_hex=QByteArray::fromHex(_trame.mid(20,4*num_sensores).toLatin1());
        ////qDebug()<<carga_util_hex[0];
        QByteArray result;
        for (int i=0;i<carga_util_hex.size();i++)
        {
            result[0]=result[0]+carga_util_hex[i];
        }
        ////qDebug()<<result.toHex()<<"resultado ";
        QString complemento="FF";
        QString unidad="1";
        QByteArray _complemento=QByteArray::fromHex(complemento.toLatin1());
        QByteArray _unidad=QByteArray::fromHex(unidad.toLatin1());

        ////qDebug()<<_complemento.toHex()<<"comeplemento";
        QByteArray final_result;
        final_result[0]=_complemento[0]-result[0]+_unidad[0];
        ////qDebug()<<final_result.toHex()<<"final_result";

        //para verificar mi check_sum comparo los valores del mismo y los de mi resultado final
        QString my_check_sum=QString(final_result.toHex());
        if(my_check_sum==check_sum)
        {
           ////qDebug()<<"son iguales";
           //dado que son iguales entonces procedo a realizar mi funcion de parseo de datos.

           //ya con la informacion debo de pasar mi cantidad de sensores a una funcion que los manejara.
           parse_sensores(_equipo,_target,num_sensores, sensores,_random,check_sum);

        }





    }//fin validadador de sensores
    else
    {
        ////qDebug()<<"cantidad de sensores de la trama, no concuerda con los sensores de la base de datos";
    }



}



//******************************************************************************************************************************
// Funcion que fragmenta la trama y la "Mapea" a los valores reales. _FRAG_BITS_NAME_NODE si los primeros 4 Bits se dividen en 2
//******************************************************************************************************************************
void jptserialbtn::trame_serial_decode(QString serial_trame){
    QStringList lv_serial_package;
    bool lv_ok = true;

    lv_serial_package.clear();

#ifdef _FRAG_BITS_NAME_NODE
    lv_serial_package.append(serial_trame.mid(def_bit_init_name_node, def_bits_frag_name_hub));
    lv_serial_package.append(serial_trame.mid(def_bit_init_name_hubs, def_bits_frag_name_hub));
#else
    lv_serial_package.append(serial_trame.mid(def_bit_init_name_node, def_bits_frag_trame));
#endif
    lv_serial_package.append(QString::number(serial_trame.mid(def_bit_init_value_sen_1, def_bits_frag_trame).toInt(&lv_ok, 16)));
    lv_serial_package.append(QString::number(serial_trame.mid(def_bit_init_value_sen_2, def_bits_frag_trame).toInt(&lv_ok, 16)));

#ifdef _HUB_ON
    #ifdef _BLINK_SERIAL
        jptblinkdata *blink = new jptblinkdata(def_serial_data);
        blink->start();
    #endif
#endif

    emit serial_data(lv_serial_package);
}

//******************************************************************************************************************************
// Funcion que busca realizar la conexion con el dispositivo asignado para la tarea de recepcion (Xbee)
//******************************************************************************************************************************
int lv_count_btn(0);
void jptserialbtn::try_connect_serial()
{
    QString lv_serial_name;
    QString lv_serial_factory;
    //imprimo lo que deseo validar
   // qDebug()<<"Lo que deseo validar";
    //qDebug()<<"a_serial_factory <<<  "<<a_serial_factory<<" !! a_serial_number   <<<"<<a_serial_number<<" || portnumber"<<a_serial_portnumber;



    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        //qDebug()<<"opcion ->"<<info.manufacturer()<<"//"<<info.serialNumber()<<"//"<<info.portName();
        if((info.manufacturer() == a_serial_factory) && (info.serialNumber() == a_serial_number) && (info.portName()==a_serial_portnumber) ){
            lv_serial_name      = info.portName();
            lv_serial_factory   = info.manufacturer();

        }
    }

    a_serial_port->setPortName(lv_serial_name);
    a_serial_port->setBaudRate(a_baud_rate);
    //qDebug()<<"Baud >>"<<a_baud_rate;
    //qDebug()<<"lv_serial_name >>"<<lv_serial_name;
    a_serial_port->setParity(QSerialPort::NoParity);
    a_serial_port->setStopBits(QSerialPort::OneStop);
    a_serial_port->setFlowControl(QSerialPort::NoFlowControl);


    if(a_serial_port->open(QIODevice::ReadWrite)){
        //qDebug()<<"Port is open";//probamos que el puerto esta en funcionamiento.
        //a_serial_port->write(QString("B").toLatin1());//
        sleep(1);
#ifdef _COMPILE_MESSAGE
            emit viewer_status("[o] Serial FTDI");//se imprime señal de funcionamiento en el viewer del monitoring
#endif
        connect(a_serial_port, &QSerialPort::readyRead, this, &jptserialbtn::read_serial_port);
        connect(a_serial_port, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(serial_unplugged()));

        gv_serial_connecting_btn= false;

        lv_count_btn = 0;
         emit status_serial(true, 0);


    }else{
        if(lv_serial_name != ""){
#ifdef _COMPILE_MESSAGE
            emit viewer_status("[!] Serial FTDI -" + lv_serial_name);
#endif
            emit status_serial(false, 0);
            ++lv_count_btn;
        }else{
#ifdef _COMPILE_MESSAGE
            emit viewer_status("[*] Serial FTDI");
#endif
            emit status_serial(false, 0);
        }
        init_timer_reconnect();
    }

}

//******************************************************************************************************************************
// SLOT que se ejecuta cuando se tiene el GUI, con el se permite cambiar de dispositivo
//******************************************************************************************************************************
void jptserialbtn::set_radio_factory_and_baud(QString serial_factory, int baud_rate){
    //qDebug()<<"======== "<< serial_factory;
    //qDebug()<<"======== "<< baud_rate;
    init_var(serial_factory);
    a_baud_rate = baud_rate;
    try_connect_serial();
}

