#ifndef JPTCONWIDGETS_H
#define JPTCONWIDGETS_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QJsonDocument>
#include <QJsonObject>

#include "class/extern/jptconsiren.h"

#include "_sentence_.h"

#define def_ip_widgets_communication    "192.168.1.24"
#define def_port_widgets_communication  10000
#define def_num_max_connections         1

class jptconwidgets : public QObject{
    Q_OBJECT
public:
    explicit jptconwidgets(QObject *parent = nullptr);

    void set_ip_clients(QStringList ip_list);
    QStringList get_ip_clients();
    void set_name_clients(QStringList name_list);
    QStringList get_name_clients();

    void set_status_client_list(QList<bool> status_list);
    QList<bool> get_status_clients_list();

    void set_send_client_list(QList<bool> send_list);
    QList<bool> get_send_client_list();

    void set_sound_time(int _time);
    int get_sound_time();

    void set_repeat_times(int _times);
    int get_repeat_times();
    //public vars clients system.
    QStringList ip_client_list;
    QStringList name_client_list;
    QList<bool> status_client_list;
    QList<bool> send_client_list;
    int sound_time;
    int repeat_time;

    jptconsiren *a_watch_status_sirens;



signals:
#ifdef _COMPILE_MESSAGE
    void viewer_status(QString status);
#endif
    void serial_data_new(QStringList data_serial);
    void status_conection_widget(QList<bool>);


private:



    QTcpServer *a_server_wd;


public slots:
    void new_connection();
    void message_from_widget();
    void disconnected();
    void send_message_to_widget(QStringList name_widget, QString name_node, QString value_1, QString value_2);
    void send_message_to_widget_raw(QString name_node, QString value_1, QString value_2);

    void send_message_to_widget_v2(QString name_node);
    void change_status_send(QList<bool> data_entry);

    void disconect_siren(int client_id,QString client_ip);



};

#endif // JPTCONWIDGETS_H
