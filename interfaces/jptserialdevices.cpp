#include "jptserialdevices.h"
#include "ui_jptserialdevices.h"
#include <QDebug>

QStringList gv_serial_device;

jptserialdevices::jptserialdevices(QWidget *parent) : QDialog(parent), ui(new Ui::jptserialdevices){
    ui->setupUi(this);
    this->setFixedSize(def_gui_fixed_width_serial,def_gui_fixed_heigth_serial);
    connect(ui->btn_refresh, SIGNAL(clicked()), this, SLOT(update_device_serial()));
    connect(this, SIGNAL(finished(int)), this, SLOT(close_device_serial()));
    update_device_serial();
}

jptserialdevices::~jptserialdevices(){
    delete ui;
}

//*********************************************************************************************************************************************************
// Actualizar los dispositivos que se encuentran conectados
//*********************************************************************************************************************************************************
void jptserialdevices::update_device_serial(){
    QStringList devices;
    devices.clear();
    devices.append("none");

    ui->modem_factory->clear();
    ui->modbus_factory->clear();
    ui->radio_factory->clear();
    ui->btn_ftdi->clear();

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        if(devices.count(info.manufacturer()) == 0)
            devices.append(info.manufacturer() + "-" + info.serialNumber()+"-"+info.portName());

    ui->modem_factory->addItems(devices);
    ui->modbus_factory->addItems(devices);
    ui->radio_factory->addItems(devices);
    ui->btn_ftdi->addItems(devices);
}
//*********************************************************************************************************************************************************
// Guardar la informacion de los dispositivos seleccionados
//*********************************************************************************************************************************************************
void jptserialdevices::close_device_serial(){
    gv_serial_device.clear();
    gv_serial_device << ui->modem_factory->currentText() <<  ui->radio_factory->currentText() << ui->modbus_factory->currentText() << ui->m_baud_rate->currentText();
    emit was_opened(gv_serial_device);
    QString macFtdi = ui->btn_ftdi->currentText();
    emit sendMacFtdi(macFtdi);
}
