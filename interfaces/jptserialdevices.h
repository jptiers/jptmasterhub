#ifndef JPTSERIALDEVICES_H
#define JPTSERIALDEVICES_H

#include <QDialog>
#include <QtSerialPort/QSerialPortInfo>

#define def_gui_fixed_width_serial     319
#define def_gui_fixed_heigth_serial    274

namespace Ui {
class jptserialdevices;
}

class jptserialdevices : public QDialog{
    Q_OBJECT

public:
    explicit jptserialdevices(QWidget *parent = 0);
    ~jptserialdevices();

private slots:
    void close_device_serial();
    void update_device_serial();

signals:
    void was_opened(QStringList serial_devices_selec);
    void sendMacFtdi(QString macFtdi);
private:
    Ui::jptserialdevices *ui;
};
#endif // JPTSERIALDEVICES_H
